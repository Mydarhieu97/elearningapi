import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Question } from 'app/models/question';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetQuestionService {

  baseurl = environment.apiUrl;

constructor(private http: HttpClient) { }


getQuestion(id: string): Observable<Question> {
  return this.http.get<Question>(this.baseurl + 'question/' + id);
}


}

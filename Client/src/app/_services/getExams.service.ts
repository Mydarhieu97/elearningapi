import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AlertifyService } from './alertify.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { Exam } from '../models/exam'
import { PaginatedResult } from 'app/models/pagination';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GetExamsService {
  baseurl = environment.apiUrl;

constructor(private http: HttpClient, private alertify: AlertifyService) { }

  getExam(id: string): Observable<Exam> {
    return this.http.get<Exam>(this.baseurl + 'exam/' + id);
  }
  
  getExams(page?, itemsPerPage?, examParams?): Observable<PaginatedResult<Exam[]>>
  {
    const paginatedResult: PaginatedResult<Exam[]> = new PaginatedResult<Exam[]>();
  let params = new HttpParams();
  if(page != null && itemsPerPage != null)
  {
    params = params.append("pageNumber", page);
    params = params.append("pageSize", itemsPerPage);
  }

  if(examParams != null)
  {

    params = params.append("disciplineID", examParams.disciplineID);
    params = params.append("moduleID", examParams.moduleID);
    

  }

  return this.http.get<Exam[]>(this.baseurl + 'exam', {observe: 'response', params})
  .pipe(
    map(response => {
      paginatedResult.result = response.body;
      if(response.headers.get('Pagination') != null)
      {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
      }
      return paginatedResult;
    })
  );
  }

}

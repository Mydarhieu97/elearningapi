import { Injectable } from '@angular/core';
import { Discipline } from 'app/models/discipline';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DisciplineService {
  baseUrlDiscipline = 'http://localhost:5000/api/discipline/';
  disciplineClass: Discipline;
  idDiscipline: any;
constructor(private http: HttpClient) { }
  getDisciplines(): Observable<Discipline[]> {
    return this.http.get<Discipline[]>(this.baseUrlDiscipline + 'getlist');
  }
  createDiscipline(model: any) {
    return this.http.post(this.baseUrlDiscipline + 'add' , model);
  }
  updateDiscipline(disciplineClass) {
    return this.http.post(this.baseUrlDiscipline + 'update', disciplineClass);
  }
  deleteDiscipline(idDiscipline) {
    return this.http.delete(this.baseUrlDiscipline + 'delete/' + idDiscipline);
  }
  getDisciplineByParent(parentId: string): Observable<Discipline[]>{
    return this.http.get<Discipline[]>(this.baseUrlDiscipline + 'getChild?parentId=' + parentId);
  }
}

/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetQuestionService } from './getQuestion.service';

describe('Service: GetQuestion', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetQuestionService]
    });
  });

  it('should ...', inject([GetQuestionService], (service: GetQuestionService) => {
    expect(service).toBeTruthy();
  }));
});

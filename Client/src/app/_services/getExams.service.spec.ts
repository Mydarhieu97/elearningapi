/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GetExamsService } from './getExams.service';

describe('Service: GetExams', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetExamsService]
    });
  });

  it('should ...', inject([GetExamsService], (service: GetExamsService) => {
    expect(service).toBeTruthy();
  }));
});

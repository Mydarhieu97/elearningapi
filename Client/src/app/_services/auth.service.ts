import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { User } from 'app/models/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = 'http://localhost:5000/api/auth/';
  jwtHelper = new JwtHelperService();
  decodeToken: any;
  currentUserName: any;
  constructor(private http: HttpClient) {}

  login(model: any) {
  return this.http.post(this.baseUrl + 'login', model)
    .pipe(
      map((response: any) => {

        const token = response;
        console.log(token.token);
        if (token) {
          localStorage.setItem('token', token.token);
          this.decodeToken = this.jwtHelper.decodeToken(token.token);
          console.log(this.decodeToken.nameid);
          console.log(this.decodeToken.role);
          this.currentUserName = this.decodeToken.unique_name;
        }
      })
    );
  }
  register(user: User) {
  return this.http.post(this.baseUrl + 'register', user);
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  roleMatch(allowRoles): Boolean {
    let isMatch = false;
    const token = localStorage.getItem('token');
    const decode = this.jwtHelper.decodeToken(token)
    const userRoles = decode.role as Array<string>;
    allowRoles.forEach(element => {
      if (userRoles.includes(element)) {
        isMatch = true;
        return;
      }
    });
    return isMatch;
  }
}


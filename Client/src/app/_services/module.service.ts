import { Injectable } from '@angular/core';
import { Module } from 'app/models/module';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {
  baseUrl = 'http://localhost:5000/api/modules/';
  moduleClass: Module
  idModule:any;
  
constructor(private http:HttpClient) { }
  getModules():Observable<Module[]>{
    return this.http.get<Module[]>(this.baseUrl+'getlist');
}
  createModule(model:any)
  {
    return this.http.post(this.baseUrl+"addmodule", model);
  }
  updateModule(moduleClass){
    return this.http.post(this.baseUrl+"update", moduleClass);
  }
  deleteModule(idModule){
    return this.http.delete(this.baseUrl+"delete/"+idModule);
  }
}




import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Exam } from 'app/models/exam';
import { Question } from 'app/models/question';
import { HttpClient } from '@angular/common/http';
import { GetQuestionService } from 'app/_services/getQuestion.service';
import { RespondStatus } from 'app/models/ElearningEnum';
import { Discipline } from 'app/models/discipline';

@Component({
  selector: 'app-examdetail',
  templateUrl: './examdetail.component.html',
  styleUrls: ['./examdetail.component.scss']
})
export class ExamdetailComponent implements OnInit {

  exam: Exam;
  questionRespond: any = {};
  questionContent: any = {};
  respondContent: any = {};
  model: any;
  respondStatus = RespondStatus;

  constructor(private route: ActivatedRoute, private http: HttpClient, private questionService: GetQuestionService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.exam = data.exam;
    });
   this.questionRespond.responds = [];

  }

  getQuestionID(item: any){
    this.questionRespond = item;
  }

  getQuestionContent(item: any){
    this.questionContent = item;
  }

  getRespondContent(item: any){
    this.respondContent = item;
  }
    
  }



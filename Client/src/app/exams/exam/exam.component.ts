import { Component, OnInit } from '@angular/core';
import { GetExamsService } from 'app/_services/getExams.service';
import { Exam } from 'app/models/exam';
import { ActivatedRoute } from '@angular/router';
import { Pagination, PaginatedResult } from 'app/models/pagination';
import { AlertifyService } from 'app/_services/alertify.service';
import { Discipline } from 'app/models/discipline';
import { Module } from 'app/models/module';
import { TestExamStatus } from 'app/models/ElearningEnum';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {

  exams: Exam[];
  examParams: any = {};
  pagination: Pagination;
  disciplines: Discipline[];
  modules: Module[];
  examStatus = TestExamStatus;

  constructor(private getExamService: GetExamsService, private route: ActivatedRoute,
    private alertify: AlertifyService) { }

  ngOnInit() {

    this.route.data.subscribe(data=>{
      this.exams = data.exams.result;
      this.pagination = data.exams.pagination;

      for(let i = 0;i<this.exams.length; i++)
      {
        const disciplineName  = data.disciplines.filter(
          book => book.disciplineID === this.exams[i].disciplineID);
        this.exams[i].disciplineName = disciplineName[0].disciplineName;

        const moduleName  = data.modules.filter(
          book => book.moduleID === this.exams[i].moduleID);
        this.exams[i].moduleName = moduleName[0].moduleName;
      }

    });

    this.route.data.subscribe(data => this.modules = data.modules);

    this.route.data.subscribe(data => this.disciplines = data.disciplines);

    this.examParams.disciplineID = "";

    this.examParams.moduleID = "";

  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadExams();
  }

  resetFilters() {
    this.examParams.disciplineID = '';
    this.examParams.moduleID = '';
    this.loadExams();
  }


  loadExams() {
    this.getExamService.getExams(this.pagination.currentPage, this.pagination.itemsPerPage,
       this.examParams).subscribe((res: PaginatedResult<Exam[]>) => {
      this.exams = res.result;
      this.pagination = res.pagination;
      for(let i = 0;i<this.exams.length; i++)
      {
        const disciplineName  = this.disciplines.filter(
          book => book.disciplineID === this.exams[i].disciplineID);
        this.exams[i].disciplineName = disciplineName[0].disciplineName;

        const moduleName  = this.modules.filter(
          book => book.moduleID === this.exams[i].moduleID);
        this.exams[i].moduleName = moduleName[0].moduleName;
      }
    }, error => {
      this.alertify.error(error);
    });
  }



}

import { Component, OnInit, Input } from '@angular/core';
import { Discipline } from 'app/models/discipline';
import { DisciplineService } from 'app/_services/discipline.service';

@Component({
  selector: 'app-part-exam',
  templateUrl: './part-exam.component.html',
  styleUrls: ['./part-exam.component.scss']
})
export class PartExamComponent implements OnInit {
  parts: Discipline[];
  private _disciplineId: string;
  @Input() set disciplineId(value: string) {
    if (value !== undefined && value) {
      this._disciplineId = value;
      this.disciplineService.getDisciplineByParent(this.disciplineId)
        .subscribe(data => this.parts = data);
    }
  }
  get disciplineId(): string {
    return this._disciplineId;
  }

  constructor(private disciplineService: DisciplineService) {
  }

  ngOnInit() {
  }

}

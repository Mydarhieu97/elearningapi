import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartExamComponent } from './part-exam.component';

describe('PartExamComponent', () => {
  let component: PartExamComponent;
  let fixture: ComponentFixture<PartExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

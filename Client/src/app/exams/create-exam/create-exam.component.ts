import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DisciplineService } from 'app/_services/discipline.service';
import { Discipline, CodeDiscipline } from 'app/models/discipline';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.css']
})
export class CreateExamComponent implements OnInit {
  disciplines: Discipline[];
  examForm: FormGroup;
  constructor(private disciplineService: DisciplineService) {
    this.disciplineService.getDisciplines().subscribe(data => {
      const disciplineFilter = data.filter(x => x.disciplineStatus === CodeDiscipline.PARENT);
      this.disciplines = disciplineFilter;
    });
    console.log(this.disciplines)
    this.examForm = new FormGroup({
      discipline: new FormControl('')
    })
  }

  ngOnInit() {
  }
  onSubmit() {
    console.log(this.examForm.get('discipline').value)
  }

}

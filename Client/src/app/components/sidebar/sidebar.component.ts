import { AuthService } from './../../_services/auth.service';
import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
  child?: RouteInfo[];
}
export const ROUTES: RouteInfo[] = [
  { path: '/admin/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: '/admin/user-profile', title: 'User Profile', icon: 'person', class: '' },
  { path: '/admin/table-list', title: 'Table List', icon: 'content_paste', class: '' },
  { path: '/admin/notifications', title: 'Notifications', icon: 'notifications', class: '' },
  { path: '/admin/discipline', title: 'Discipline Manager', icon: 'person', class: '' },
  { path: '/admin/module', title: 'Module Manager', icon: 'person', class: '' },
  {
    path: '/admin/exams', title: 'Exam Manager', icon: 'class', class: '', child: [
      {
        path: '/admin/exam/all', title: 'All Exam', icon: 'class', class: ''
      },
      {
        path: '/admin/exam/create', title: 'Create Exam', icon: 'class', class: ''
      },
    ]
  },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: RouteInfo[];

  constructor(private autheService: AuthService) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
  roleMatch() {
    return this.autheService.roleMatch(['Admin']);
  }
}

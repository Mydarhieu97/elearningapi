export enum TestExamStatus {
    Inactive = 0,
    JustCreated = 1,
    AwaitingApproval = 2,
    Approved = 3,
    CanceledApproval = 4
}

export enum RespondStatus {
    Inactive = 0,
    Active = 1
}

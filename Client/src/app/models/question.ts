import {Respond} from './respond';

export interface Question {
    questionID: string;
    questionContent: string;
    mediaUrl: string;
    createdDate: Date;
    personCreated: string;
    isLock: boolean;
    questionTypeID: string;
    responds?: Respond[];
}

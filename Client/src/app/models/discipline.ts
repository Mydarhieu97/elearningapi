export class Discipline {
    disciplineName: string;
    disciplineID: string;
    disciplineStatus: number;
    disciplineCode: string;
    parentId: string;
    description: string;
}
export class CodeDiscipline {
    public static Speaking = 'speak';
    public static Listening = 'listen';
    public static Reading = 'read';
    public static Writing = 'write';
    public static PARENT = 1;
}

import {Question} from './question';

export interface Exam {
    testExamID: string;
    testExamName: string;
    testExamStatus: number;
    disciplineName: string;
    moduleName: string;
    disciplineID: string;
    moduleID: string;
    questions?: Question[];
}
export interface Respond {
    respondID: string;
    respondContent: string;
    isTrue: boolean;
    questionID: string;
    mediaUrl: string;
    respondStatus: number;
}

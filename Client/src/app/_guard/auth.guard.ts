import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private routes: Router, private alertify: AlertifyService) {}
  canActivate(next: ActivatedRouteSnapshot): boolean {
    const roles = next.firstChild.data.role as Array<string>;
    console.log(roles);
    if (roles) {
      const match = this.authService.roleMatch(roles);
      if (match) {
        return true;
      } else {
        this.routes.navigate(['']);
        this.alertify.error('Authorised to access');
      }
    }

    if (this.authService.loggedIn()) {
      return true;
    } else {
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      this.alertify.error('You must login!! Dont try it again!');
      this.routes.navigate(['']);
      return false;
    }
  }
}

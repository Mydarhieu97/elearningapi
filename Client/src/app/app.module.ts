import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { ClientLayoutComponent } from './layouts/client-layout/client-layout.component';
import { GetExamsService } from './_services/getExams.service';
import { ListDisciplineResolver } from './_resolvers/discipline.resolver';
import { AlertifyService } from './_services/alertify.service';
import { ListModuleResolver } from './_resolvers/module.resolver';
import { NavComponent } from './layouts/client-layout/nav/nav.component';
import { JwtModule } from '@auth0/angular-jwt';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { AdminComponent } from './layouts/admin-layout/admin/admin.component';
import { AuthGuard } from './_guard/auth.guard';
import { AdminService } from './_services/admin.service';
import { ModalModule } from 'ngx-bootstrap/modal';
import { RoleModalComponent } from './layouts/admin-layout/role-modal/role-modal.component';
import { MatCheckboxModule } from '@angular/material';
import { RegisterComponent } from './layouts/client-layout/register/register.component';

export function tokkenGetter() {
   return localStorage.getItem('token');
}

@NgModule({
   imports: [
      BrowserModule,
      HttpClientModule,
      BrowserAnimationsModule,
      FormsModule,
      ReactiveFormsModule,
      HttpModule,
      ComponentsModule,
      RouterModule,
      AppRoutingModule,
      JwtModule.forRoot({
         config: {
            tokenGetter: tokkenGetter,
            whitelistedDomains: ['localhost:5000'],
            blacklistedRoutes: ['localhost:5000/api/auth']
          }
      }),
      BsDropdownModule.forRoot(),
      ModalModule.forRoot(),
      MatCheckboxModule
   ],
   declarations: [
      AppComponent,
      AdminLayoutComponent,
      ClientLayoutComponent,
      NavComponent,
      RegisterComponent,
      RoleModalComponent,
   ],
   providers: [
      ListDisciplineResolver,
      ListModuleResolver,
      AlertifyService,
      AuthGuard,
      AdminService,
   ],
   bootstrap: [
      AppComponent
   ],
   entryComponents: [
      RoleModalComponent,
      RegisterComponent
     ]
})
export class AppModule { }

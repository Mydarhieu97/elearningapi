import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { PaginationModule } from 'ngx-bootstrap';
import { NgSelect2Module } from 'ng-select2';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatMenuModule,
  MatTabGroup,
  MatTab
} from '@angular/material';
import { DisciplineComponent } from 'app/discipline/discipline.component';
import { ModuleComponent } from 'app/module/module.component';
import { AlertifyService } from 'app/_services/alertify.service';
import { DisciplineService } from 'app/_services/discipline.service';
import { ListDisciplineResolver } from 'app/_resolvers/discipline.resolver';
import { CreateExamComponent } from 'app/exams/create-exam/create-exam.component';
import { ExamComponent } from 'app/exams/exam/exam.component';
import { ExamdetailComponent } from 'app/exams/examdetail/examdetail.component';
import { ExamResolver } from 'app/_resolvers/exam.resolver';
import { ExamsResolver } from 'app/_resolvers/exams.resolver';
import { AdminComponent } from './admin/admin.component';
import { MatTabsModule } from '@angular/material';
import { UserManagementComponent } from './user-management/user-management.component';
import { RoleModalComponent } from './role-modal/role-modal.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PartExamComponent } from 'app/exams/create-exam/part-exam/part-exam.component';


import { ModuleService } from 'app/_services/module.service';
import { ListModuleResolver } from 'app/_resolvers/module.resolver';
import { GetQuestionService } from 'app/_services/getQuestion.service';

@NgModule({
  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatMenuModule,
    MatTabsModule,
    PaginationModule.forRoot(),
    MatCheckboxModule,
    NgSelect2Module
  ],
  declarations: [
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    NotificationsComponent,
    DisciplineComponent,
    ModuleComponent,
    CreateExamComponent,
    ExamComponent,
    ExamdetailComponent,
    AdminComponent,
    UserManagementComponent,
    ExamdetailComponent,
    PartExamComponent,
  ],
  providers: [
    AlertifyService,
    ExamResolver,
    ExamsResolver,
    GetQuestionService
 ],

})

export class AdminLayoutModule {}

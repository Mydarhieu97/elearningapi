import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { DisciplineComponent } from 'app/discipline/discipline.component';
import { ModuleComponent } from 'app/module/module.component';
import { ListDisciplineResolver } from 'app/_resolvers/discipline.resolver';
import { ListModuleResolver } from 'app/_resolvers/module.resolver';
import { ExamComponent } from 'app/exams/exam/exam.component';
import { CreateExamComponent } from 'app/exams/create-exam/create-exam.component';
import { ExamdetailComponent } from 'app/exams/examdetail/examdetail.component';
import { ExamResolver } from 'app/_resolvers/exam.resolver';
import { ExamsResolver } from 'app/_resolvers/exams.resolver';
import { AuthGuard } from '../../_guard/auth.guard';
import { AdminComponent } from './admin/admin.component';

export const AdminLayoutRoutes: Routes = [

    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children:
            [{ path: '', component: DashboardComponent },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'rolesmanager', component: AdminComponent, data: { role: ['Admin'] } },
            { path: 'user-profile', component: UserProfileComponent },
            { path: 'table-list', component: TableListComponent },
            { path: 'notifications', component: NotificationsComponent },
            { path: 'discipline', component: DisciplineComponent, resolve: { disciplines: ListDisciplineResolver } },
            { path: 'module', component: ModuleComponent, resolve: { modules: ListModuleResolver } },
            {
                path: 'exam', children: [
                    {
                        path: 'all', component: ExamComponent, resolve: { exams: ExamsResolver, disciplines: ListDisciplineResolver, modules: ListModuleResolver }
                    },
                    {
                        path: 'create', component: CreateExamComponent
                    },
                    { path: ':id', component: ExamdetailComponent, resolve: { exam: ExamResolver } },
                    
                ]
            }]
    },
];

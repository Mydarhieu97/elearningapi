import { Component, OnInit } from '@angular/core';
import { AlertifyService } from 'app/_services/alertify.service';
import { AuthService } from 'app/_services/auth.service';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { RegisterComponent } from '../register/register.component';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  model: any = {};
  photoUrl: string;
  bsModalRef: BsModalRef;
  constructor(public authService: AuthService, private alertify: AlertifyService, private routes: Router,
              private modalService: BsModalService) { }

  ngOnInit() {
  }
  login() {
    this.authService.login(this.model).subscribe(next => {
      this.alertify.success('Login successfully');
      console.log(this.authService.currentUserName);
    }, error => {
      this.alertify.error(error);
    }, () => {
      this.routes.navigate(['/client']);
    });
  }
  loggedIn() {
    return this.authService.loggedIn();
  }
  logout() {
    localStorage.removeItem('token');
    this.authService.decodeToken = null;
    this.alertify.message('Logged out!');
    this.routes.navigate(['/client']);
  }

  registerModal() {
    const initialState = {
    };
    this.bsModalRef = this.modalService.show(RegisterComponent, { initialState });
  }
}

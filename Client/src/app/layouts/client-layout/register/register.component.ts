import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/_services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertifyService } from 'app/_services/alertify.service';
import { User } from 'app/models/user';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user: User;
  registerForm: FormGroup;
  constructor(private authService: AuthService, private alerttify: AlertifyService,
    private fb: FormBuilder, private route: Router, public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.createRegisterForm();
  }

  createRegisterForm() {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(4)]],
      confirmPassword: ['', Validators.required]
    }, {validators: this.checkPassword});
  }

  checkPassword(fg: FormGroup) {
    return fg.get('password').value === fg.get('confirmPassword').value ? null : {mismatch: true};
  }

  register() {
    if (this.registerForm.valid) {
      this.user = Object.assign({}, this.registerForm.value);
      this.authService.register(this.user).subscribe(() => {
        this.alerttify.success('register successful');
      }, error => {
        this.alerttify.error(error);
      }, () => {
        this.authService.login(this.user).subscribe(() => {
          this.route.navigate(['']);
          this.bsModalRef.hide();
        });
      });
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { ModuleService } from 'app/_services/module.service';
import { AlertifyService } from 'app/_services/alertify.service';
import { ActivatedRoute, ActivatedRouteSnapshot, RouterModule, Router } from '@angular/router';
import { Module } from 'app/models/module';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss']
})
export class ModuleComponent implements OnInit {
  tempModelForAdd: any = {};
    modules: Module[];
    oneModuleUpdate: any = {};
    idModuleDelete: any;
    itemSelect: any;
  constructor(private moduleService: ModuleService, private route: ActivatedRoute, private router: Router,
    private alertify: AlertifyService, private http: HttpClient ) { }
  ngOnInit() {
    this.route.data.subscribe(data => {
      this.modules = data.modules;
    });
  }
  createoneModule() {
    this.moduleService.createModule(this.tempModelForAdd).subscribe(
      () => {
        const newObj = new Module();
        newObj.moduleName = this.tempModelForAdd.moduleName;
        this.modules.push(newObj);
        this.tempModelForAdd.moduleName === '';
        this.alertify.success('Add module success');
      }
    );
  }

  confirmDelete(modules) {
    this.alertify.confirm('Are you sure Delete this module ?', () => {
      this.deleteModule(modules.moduleID);
      const index: number = this.modules.indexOf(modules);
      if (index !== -1) {
        this.modules.splice(index, 1);
      }
    });
  }
  updateModule() {
    this.moduleService.updateModule(this.oneModuleUpdate).subscribe(
      () => {
        this.alertify.success('Update Success');

        // for(let m=0;m< this.modules.length;m++){
        //   if(this.modules.indexOf())
        // }


        // let newObj=new Module();
        // newObj.moduleName=this.oneModuleUpdate.moduleName;
        // this.modules.push(newObj);
      }
    );


  }
  deleteModule(idModuleDelete) {
    this.moduleService.deleteModule(idModuleDelete).subscribe(
      () => {
        this.alertify.success('Delete Success');
      }
    );
  }
  sendIdModule(id, name) {
    this.oneModuleUpdate.moduleID = id;
    this.oneModuleUpdate.moduleName = name;
    this.oneModuleUpdate.moduleStatus = 1;
  }
  onOptionsSelected(value: string) {
    console.log('the selected value is ' + value);
  }
}

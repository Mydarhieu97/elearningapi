import { Injectable, ErrorHandler } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot , ActivatedRoute} from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Discipline } from 'app/models/discipline';
import { DisciplineService } from 'app/_services/discipline.service';

@Injectable()
export class ListDisciplineResolver implements Resolve<Discipline[]> {
    constructor(private disciplineService: DisciplineService,
                private router: Router, private alertify: AlertifyService) {}
    resolve(route: ActivatedRouteSnapshot): Observable<Discipline[]> {
        return this.disciplineService.getDisciplines().pipe(
            catchError(error => {
                this.alertify.error('Problem retrieving data, maybe api not working');
                this.router.navigate(['/admin']);
                return of(null);
            })
        );
    }
}




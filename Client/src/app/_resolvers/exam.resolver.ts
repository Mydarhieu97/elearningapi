import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot , ActivatedRoute} from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Module } from 'app/models/module';
import { ModuleService } from 'app/_services/module.service';
import { Exam } from 'app/models/exam';
import { GetExamsService } from 'app/_services/getExams.service';
@Injectable()
export class ExamResolver implements Resolve<Exam> {
    constructor(private examService: GetExamsService,
                private router: Router, private alertify: AlertifyService) {}
    resolve(route: ActivatedRouteSnapshot): Observable<Exam> {
        return this.examService.getExam(route.params['id']).pipe(
            catchError(error => {
                this.alertify.error(error.error.split(":")[1].split('\n')[0]);
                this.router.navigate(['/admin/exam']);
                return of(null);
            })
        );
    }
}
import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot , ActivatedRoute} from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Module } from 'app/models/module';
import { ModuleService } from 'app/_services/module.service';
@Injectable()
export class ListModuleResolver implements Resolve<Module[]> {
    constructor(private moduleService: ModuleService,
                private router: Router, private alertify: AlertifyService) {}
    resolve(route: ActivatedRouteSnapshot): Observable<Module[]> {
        return this.moduleService.getModules().pipe(
            catchError(error => {
                this.alertify.error('Problem retrieving data, maybe api not working');
                this.router.navigate(['/admin']);
                return of(null);
            })
        );
    }
}
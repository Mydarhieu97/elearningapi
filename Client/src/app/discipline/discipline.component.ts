import { Component, OnInit } from '@angular/core';
import { DisciplineService } from 'app/_services/discipline.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Discipline } from 'app/models/discipline';
import { AlertifyService } from 'app/_services/alertify.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-discipline',
  templateUrl: './discipline.component.html',
  styleUrls: ['./discipline.component.scss']
})
export class DisciplineComponent implements OnInit {
  disciplineFormForCreate: FormGroup;
  tempModelForAdd: any = {};
  oneDisciplineUpdate: any = {};
  disciplines: Discipline[];
  constructor(private disciplineservice: DisciplineService, private http: HttpClient,
    private alertify: AlertifyService, private fb: FormBuilder,
    private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.data.subscribe(data => {
      this.disciplines = data.disciplines;
    });
    this.createDisciplineForm();
  }

   //#region  CRUD Discipline
  createoneDiscipline() {
    this.disciplineservice.createDiscipline(this.tempModelForAdd).subscribe(
      () => {
        const newObj = new Discipline();
        newObj.disciplineName = this.tempModelForAdd.disciplineName;
        this.disciplines.push(newObj);
        this.tempModelForAdd.disciplineName === "";
        this.alertify.success('Add Discipline success');
      }
    );
  }

  confirmDelete(disciplines) {
    this.alertify.confirm('Are you sure Delete this discipline ?', () => {
      this.deleteDiscipline(disciplines.disciplineID);
      const index: number = this.disciplines.indexOf(disciplines);
      if (index !== -1) {
        this.disciplines.splice(index, 1);
      }
    });
  }

  updateDiscipline() {
    this.disciplineservice.updateDiscipline(this.oneDisciplineUpdate).subscribe(
      () => {
        this.alertify.success('Update Success');
      }
    );
  }

  deleteDiscipline(idDisciplineDelete) {
    this.disciplineservice.deleteDiscipline(idDisciplineDelete).subscribe(
      () => {
        this.alertify.success('Delete Success');
      }
    );
  }


  sendIdDiscipline(id, name) {
    this.oneDisciplineUpdate.disciplineID = id;
    this.oneDisciplineUpdate.disciplineName = name;
    this.oneDisciplineUpdate.disciplineStatus = 1;
    // this.oneDisciplineUpdate.disciplineCode="";
  }

  // #endregion CRUD Discipline
  createDisciplineForm() {
    this.disciplineFormForCreate = this.fb.group({
      parentId: [''],
      DisciplineName: ['', Validators.required],
      DisciplineCode: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(4)]],
      description: ['', [Validators.required, Validators.maxLength(200)]]
    })
  }
  onOptionsSelected(value: string) {
    console.log('the selected value is ' + value);
  }
  createDiscipline01() {
    if (this.disciplineFormForCreate.valid) {
      this.disciplineservice.createDiscipline(Object.assign({}, this.disciplineFormForCreate.value)).subscribe(() => {
        this.alertify.success('Create Discipline successful');
      }, error => {
        this.alertify.error(error);
      });
    }
  }
}

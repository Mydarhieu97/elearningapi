﻿using Elearning.Infrastructure;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Helpers
{
    public class Seed
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public Seed(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void SeedUser()
        {
            var roles = new List<ApplicationRole>
            {
                new ApplicationRole{Name = "Admin"},
                new ApplicationRole{Name = "User"},
                new ApplicationRole{Name = "Marker"},
                new ApplicationRole{Name = "Author"}
            };

            foreach (var role in roles)
            {
                _roleManager.CreateAsync(role).Wait();
            }

            var adminUser = new ApplicationUser
            {
                UserName = "Admin"
            };
            _userManager.CreateAsync(adminUser, "password").Wait();
            _userManager.AddToRoleAsync(adminUser, "Admin").Wait();

            var makerUser = new ApplicationUser
            {
                UserName = "Marker"
            };
            _userManager.CreateAsync(makerUser, "password").Wait();
            _userManager.AddToRoleAsync(makerUser, "Marker").Wait();

            var authorUser = new ApplicationUser
            {
                UserName = "Author"
            };
            _userManager.CreateAsync(authorUser, "password").Wait();
            _userManager.AddToRolesAsync(authorUser, new[] {"Author", "Marker" } ).Wait();

            var user = new ApplicationUser
            {
                UserName = "UserName"
            };
            _userManager.CreateAsync(user, "password").Wait();
            _userManager.AddToRoleAsync(user, "User").Wait();
        }
    }
}

﻿using AutoMapper;
using Elearning.API.Models;
using Elearning.Application.ViewModels;
using Elearning.Domain.Extended;
using Elearning.Infrastructure;
using Elearning.Infrastructure.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserForRegisterDto, ApplicationUser>();
            CreateMap<ApplicationUser, UserForDetailDto>();
            CreateMap<Discipline, DisciplineViewModel>();
            CreateMap<TestExam, ListExamViewModel >();
            CreateMap(typeof(PagedList<TestExam>), typeof(PagedList<ListExamViewModel>)).ConvertUsing(typeof(PagedListConverter<TestExam, ListExamViewModel>));
            //CreateMap<PagedList<TestExam>, PagedList<ListExamViewModel>>();

        }


    }


    public class PagedListConverter<TSource, TDestination> : ITypeConverter<PagedList<TSource>, PagedList<TDestination>> where TSource : class where TDestination : class
    {
        public PagedList<TDestination> Convert(PagedList<TSource> source, PagedList<TDestination> destination, ResolutionContext context)
        {
            var collection = context.Mapper.Map<List<TSource>, List<TDestination>>(source);

            return new PagedList<TDestination>(collection, source.TotalCount, source.CurrentPage, source.PageSize);
        }
    }

    
}

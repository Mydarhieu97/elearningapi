﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Models
{
    public class DisciplineForAdd
    {
        public Guid DisciplineID { get; set; }
        public string DisciplineName { get; set; }
        public int DisciplineStatus { get; set; }
        public string DisciplineCode { get; set; } 
        public Guid ParentID { get; set; }
        public string Description { get; set; }
    }
    public class DisciplineForUpdate
    {
        public Guid DisciplineID { get; set; }
        public string DisciplineName { get; set; }
        public int DisciplineStatus { get; set; }
        public string DisciplineCode { get; set; }
        public Guid ParentID { get; set; }
        public string Description { get; set; }
    }

}

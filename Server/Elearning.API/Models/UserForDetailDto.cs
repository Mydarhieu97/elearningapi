﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Models
{
    public class UserForDetailDto
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }
    }
}

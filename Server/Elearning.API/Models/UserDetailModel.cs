﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Models
{
    public sealed class UserDetailModel
    {
        public Guid Id { get; }
        public string Name { get; }
        public string PhoneNumber { get; }

        public UserDetailModel(Guid id, string name, string phoneNumber)
        {
            Id = id;
            Name = name;
            PhoneNumber = phoneNumber;
        }
    }
}

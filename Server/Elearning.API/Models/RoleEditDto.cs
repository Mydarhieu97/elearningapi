﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Models
{
    public class RoleEditDto
    {
        public string[] RoleNames { get; set; }
    }
}

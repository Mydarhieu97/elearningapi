﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.API.Models
{
    public class ModuleForAdd
    {
        public Guid ModuleID { get; set; }
        public string ModuleName { get; set; }
        public int ModuleStatus { get; set; }
    }
}

using AutoMapper;
using Elearning.API.Helpers;
using Elearning.Application.Commands.Discipline;
using Elearning.Application.Commands.Module;
using Elearning.Application.Commands.Order;
using Elearning.Application.Queries;
using Elearning.Application.Repositories;
using Elearning.Infrastructure;
using Elearning.Infrastructure.EntityFrameworkDataAccess.Queries;
using Elearning.Infrastructure.EntityFrameworkDataAccess.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Elearning.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ElearningContext>(x => x.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddTransient<IExamQueries, ExamQueries>();
            //discipline
            services.AddTransient<IDisciplineReadOnlyRepository, DisciplineRepositories>();
            services.AddTransient<IDisciplineWriteOnlyRepository, DisciplineRepositories>();
            services.AddTransient<IDisciplineUseCase, DisciplineUseCase>();
            services.AddTransient<IDisciplineQueries, DisciplineQueries>();
            //modules
            services.AddTransient<IModuleReadOnlyRepository, ModuleRepositories>();
            services.AddTransient<IModuleWriteOnlyRepository, ModuleRepositories>();
            services.AddTransient<IModuleUseCase, ModuleUseCase>();
            services.AddTransient<IModuleQueries, ModuleQueries>();
            //exam
            services.AddTransient<IExamQueries, ExamQueries>();
            //question
            services.AddTransient<IQuestionQueries, QuestionQueries>();
            services.AddTransient<IRespondQueries, RespondQueries>();
            //Order
            services.AddTransient<IOrderWriteOnlyRepository, OrderRepository>();
            services.AddTransient<IOrderReadOnlyRepository, OrderRepository>();
            services.AddTransient<IOrderQueries, OrderQueries>();
            services.AddTransient<IOrderUseCase, OrderUseCase>();
            //Users
            services.AddTransient<Seed>();
            IdentityBuilder builder = services.AddIdentityCore<ApplicationUser>(opt =>
            {
                opt.Password.RequireDigit = false;
                opt.Password.RequiredLength = 6;
                opt.Password.RequireNonAlphanumeric = false;
                opt.Password.RequireUppercase = false;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(ApplicationRole), builder.Services);
            builder.AddEntityFrameworkStores<ElearningContext>();
            builder.AddRoleValidator<RoleValidator<ApplicationRole>>();
            builder.AddRoleManager<RoleManager<ApplicationRole>>();
            builder.AddSignInManager<SignInManager<ApplicationUser>>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(option =>
                {
                    option.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII
                            .GetBytes(Configuration.GetSection("AppSettings:Token").Value)),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddAuthorization(opt =>
            {
                opt.AddPolicy("RequireAdminRole", policy => policy.RequireRole("Admin"));
                //opt.AddPolicy("RequireAdminRole", policy => policy.RequireRole("Admin"));
                //opt.AddPolicy("RequireAdminRole", policy => policy.RequireRole("Admin"));
            });

            services.AddControllers(
                opt =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                //opt.Filters.Add(new AuthorizeFilter(policy));
            }
            );

            services.AddCors();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapperProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, Seed seed)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            //open when want to seed defualt user with roles
            //seed.SeedUser();

            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

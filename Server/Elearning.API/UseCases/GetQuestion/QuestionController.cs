﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Elearning.API.UseCases.GetQuestion
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IQuestionQueries _questionQueries;
        public QuestionController(IQuestionQueries questionQueries)
        {
            _questionQueries = questionQueries;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetQuestion(Guid id)
        {
            QuestionViewModel question = await _questionQueries.GetQuestion(id);

            if (question == null)
            {
                return new NoContentResult();
            }

            return new ObjectResult(question);
        }
        //[HttpGet]
        //public async Task<IActionResult> GetExams([FromQuery]ExamParams examParams)
        //{

        //    var exams = await _examQueries.GetExams(examParams);
        //    Response.AddPagination(exams.CurrentPage, exams.PageSize, exams.TotalCount, exams.TotalPages);
        //    return Ok(exams);

        //}

    }
}
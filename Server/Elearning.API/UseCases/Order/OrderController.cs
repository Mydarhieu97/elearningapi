﻿using Elearning.Application.Commands.Order;
using Elearning.Application.Queries;
using Elearning.Application.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Elearning.API.UseCases.Order
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderUseCase _orderServices;
        private readonly IOrderQueries _orderQueries;
        private readonly IOrderReadOnlyRepository _orderReadRepos;
        private readonly IOrderWriteOnlyRepository _orderWriteRepos;

        public OrderController(IOrderUseCase orderServices,
            IOrderQueries orderQueries,
            IOrderReadOnlyRepository orderReadRepos,
            IOrderWriteOnlyRepository orderWriteRepos)
        {
            _orderServices = orderServices;
            _orderQueries = orderQueries;
            _orderReadRepos = orderReadRepos;
            _orderWriteRepos = orderWriteRepos;
        }

        [AllowAnonymous]
        [HttpPost("add")]
        public async Task<IActionResult> Add(OrderRequest req)
        {
            var res = await _orderServices.Execute(req.UserId, req.TestExamIds);

            if (res == null) return BadRequest();

            return Ok(res);
        }

        [AllowAnonymous]
        [HttpGet("get")]
        public async Task<IActionResult> GetAll()
        {
            var orders = await _orderQueries.GetAll();

            if (orders == null) return BadRequest();

            return Ok(orders);
        }

        [AllowAnonymous]
        [HttpGet("get/{orderId}")]
        public async Task<IActionResult> GetSingle(string orderId)
        {
            var order = await _orderQueries.Get(Guid.Parse(orderId));

            if (order == null) return BadRequest();

            return Ok(order);
        }

        [AllowAnonymous]
        [HttpGet("getByUser/{userId}")]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            var orders = await _orderQueries.GetByUserId(Guid.Parse(userId));

            if (orders == null) return BadRequest();

            return Ok(orders);
        }

        [AllowAnonymous]
        [HttpPut("delete/{orderIdForDelete}")]
        public async Task<IActionResult> DeleteOrder(string orderIdForDelete)
        {
            var order = await _orderReadRepos.Get(Guid.Parse(orderIdForDelete));

            if (order == null) return BadRequest();

            await _orderWriteRepos.Delete(order);

            return Ok(order);
        }
    }
}
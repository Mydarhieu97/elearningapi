﻿using System.Collections.Generic;

namespace Elearning.API.UseCases.Order
{
    public class OrderRequest
    {
        public string UserId { get; set; }
        public List<string> TestExamIds { get; set; }
    }
}

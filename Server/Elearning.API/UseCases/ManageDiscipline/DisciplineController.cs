﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elearning.API.Models;
using Elearning.Application.Commands.Discipline;
using Elearning.Application.Queries;
using Elearning.Application.Repositories;
using Elearning.Application.ViewModels;
using Elearning.Domain.DisciplineDomain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Elearning.API.UseCases.AddDiscipline
{
    [Route("api/[controller]")]
    [ApiController]
   
    public class DisciplineController : ControllerBase
    {
        private readonly IDisciplineWriteOnlyRepository _disciplineWriteOnlyRepository;
        private readonly IDisciplineReadOnlyRepository _disciplineReadOnlyRepository;
        private readonly IDisciplineUseCase _disciplineUseCase;
        private readonly IDisciplineQueries _disciplineQueries;
        public DisciplineController(IDisciplineWriteOnlyRepository disciplineWriteOnly, IDisciplineReadOnlyRepository disciplineReadOnlyRepository
            , IDisciplineUseCase disciplineUseCase, IDisciplineQueries disciplineQueries)
        {
            _disciplineWriteOnlyRepository = disciplineWriteOnly;
            _disciplineReadOnlyRepository = disciplineReadOnlyRepository;
            _disciplineUseCase = disciplineUseCase;
            _disciplineQueries = disciplineQueries;  
        }
        [HttpPost("add")]
        public async Task<IActionResult> AddDiscipline([FromBody]DisciplineForAdd disciplineForAdd )
        {
            DisciplineDTO  result = await _disciplineUseCase.Execute(disciplineForAdd.DisciplineName, disciplineForAdd.DisciplineCode,
                disciplineForAdd.Description,(disciplineForAdd.ParentID).ToString());
            return Ok();
        }
        [HttpGet("getlist")]
        public async Task<List<DisciplineViewModel>> GetListDiscipline()
        {
            List<DisciplineViewModel> listDiscipline = new List<DisciplineViewModel>();
            listDiscipline = await _disciplineQueries.GetListDiscipline();
            foreach(var item in listDiscipline)
            {
                Guid id = item.DisciplineID;
            }
            return listDiscipline;
        }
        [HttpPost("update")]
        public async Task<IActionResult> updateDiscipline([FromBody]DisciplineForUpdate disciplineForUpdate)
        {
             if (ModelState.IsValid)
            {
                await _disciplineWriteOnlyRepository.Update(disciplineForUpdate.DisciplineID, disciplineForUpdate.DisciplineName,
                    disciplineForUpdate.DisciplineStatus,disciplineForUpdate.DisciplineCode);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> deleteDiscipline(Guid id)
        {

            if (ModelState.IsValid)
            {
                await _disciplineWriteOnlyRepository.Delete(id);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }

        [HttpGet("getChild")]
        public async Task<IActionResult> GetDisciplineByParent([FromQuery]Guid parentId)
        {
            if(parentId != null)
            {
                return Ok(await _disciplineQueries.GetDisciplineByParent(parentId));
            }
            return NotFound();
        }
    }
}
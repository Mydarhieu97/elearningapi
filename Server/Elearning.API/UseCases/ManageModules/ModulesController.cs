﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elearning.API.Models;
using Elearning.Application.Commands.Module;
using Elearning.Application.Queries;
using Elearning.Application.Repositories;
using Elearning.Application.ViewModels;
using Elearning.Domain.ModuleDomain;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Elearning.API.UseCases.ManageModules
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModulesController : ControllerBase
    {
        private readonly IModuleWriteOnlyRepository _moduleWriteOnlyRepository;
        private readonly IModuleReadOnlyRepository _moduleReadOnlyRepository;
        private readonly IModuleUseCase _moduleUseCase;
        private readonly IModuleQueries _moduleQueries;
        public ModulesController(IModuleWriteOnlyRepository moduleWriteOnlyRepository,
            IModuleReadOnlyRepository moduleReadOnlyRepository, IModuleUseCase moduleUseCase,
            IModuleQueries moduleQueries)
        {
            _moduleWriteOnlyRepository = moduleWriteOnlyRepository;
            _moduleReadOnlyRepository = moduleReadOnlyRepository;
            _moduleUseCase = moduleUseCase;
            _moduleQueries = moduleQueries;
        }

        [HttpPost("addmodule")]
        public async Task<IActionResult> AddModule([FromBody]ModuleForAdd moduleForAdd)
        {
            ModuleDTO result = await _moduleUseCase.Execute(moduleForAdd.ModuleName);
            return Ok();
        }
        [HttpGet("getlist")]
        public async Task<List<ModuleViewModel>> GetListModule()
        {
            List<ModuleViewModel> listModule = new List<ModuleViewModel>();
            listModule = await _moduleQueries.GetListModule();
            foreach (var item in listModule)
            {
                Guid idnew = item.ModuleID;
            }
            return listModule;
        }
        [HttpPost("update")]
        public async Task<IActionResult> updateModule([FromBody]ModuleForAdd moduleForAdd)
        {
            if (ModelState.IsValid)
            {
                await _moduleWriteOnlyRepository.Update(moduleForAdd.ModuleID, moduleForAdd.ModuleName);
                return Ok();
            }
            else
            {
                return NotFound(); 
            }
        }
        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> deleteModule(Guid id)
        {
           
            if (ModelState.IsValid)
            {
                await _moduleWriteOnlyRepository.Delete(id);
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
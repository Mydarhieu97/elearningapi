﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elearning.API.Helpers;
using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using Elearning.Domain.Extended;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Elearning.API.UseCases.GetExamsByAuthor
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamController : ControllerBase
    {
        private readonly IExamQueries _examQueries;
        public ExamController(IExamQueries examQueries)
        {
            _examQueries = examQueries;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetExam(Guid id)
        {
            ExamViewModel exam = await _examQueries.GetExam(id);

            if (exam == null)
            {
                return new NoContentResult();
            }

            return new ObjectResult(exam);
        }
        [HttpGet]
        public async Task<IActionResult> GetExams([FromQuery]ExamParams examParams)
        {

            var exams = await _examQueries.GetExams(examParams);
            Response.AddPagination(exams.CurrentPage, exams.PageSize, exams.TotalCount, exams.TotalPages);
            return Ok(exams);

            //List<ListExamViewModel> exams = await _examQueries.GetExams();

            //return exams;
        }

    }
}
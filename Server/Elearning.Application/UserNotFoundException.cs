﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application
{
    internal sealed class UserNotFoundException : ApplicationException
    {
        internal UserNotFoundException(string message)
            : base(message)
        { }
    }
}

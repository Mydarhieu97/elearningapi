﻿using Elearning.Domain.TestExams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
    public interface IExamReadOnlyRepository
    {
        Task<Exams> GetExam(Guid id);
        List<Exams> GetExams();
    }
}

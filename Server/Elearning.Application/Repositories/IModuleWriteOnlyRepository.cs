﻿using Elearning.Application.ViewModels;
using Elearning.Domain.ModuleDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
   public interface IModuleWriteOnlyRepository
    {
        Task Add(ModuleDomain moduleDomain);
        Task Update(Guid idModule, string moduleName);
        Task Delete(Guid id);
    }
}

﻿using Elearning.Domain.DisciplineDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
    public interface IDisciplineWriteOnlyRepository
    {
        Task Add(DisiplineDomain discipline);
        Task Update(Guid disciplineIDUpdate, string disciplineNameUpdate, int disciplineStatusUpdate, string disciplineCodeUpdate);
        Task Delete(Guid id);
    }
}

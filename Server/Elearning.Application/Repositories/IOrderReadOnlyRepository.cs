﻿using Elearning.Domain.Orders;
using System;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
    public interface IOrderReadOnlyRepository
    {
        Task<Order> Get(Guid orderId);
    }
}

﻿using Elearning.Domain.ModuleDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
   public interface IModuleReadOnlyRepository
    {
        Task<ModuleDomain> GetOneModule(Guid id);
        List<ModuleDomain> GetListModule();
    }
}

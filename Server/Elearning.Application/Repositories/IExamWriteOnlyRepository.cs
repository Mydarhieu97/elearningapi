﻿using Elearning.Domain.TestExams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
    public interface IExamWriteOnlyRepository
    {
        Task Add(Exams exam);
        Task Update(Exams exam);
        Task Delete(Guid id);
    }
}

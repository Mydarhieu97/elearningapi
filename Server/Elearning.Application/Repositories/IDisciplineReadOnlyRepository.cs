﻿using Elearning.Domain.DisciplineDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
   public interface IDisciplineReadOnlyRepository
    {
        Task<DisiplineDomain> GetOneDiscipline(Guid id);
        List<DisiplineDomain> GetListDiscipline();
    }
}

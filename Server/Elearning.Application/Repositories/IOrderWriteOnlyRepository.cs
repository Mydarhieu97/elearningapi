﻿using Elearning.Domain.Orders;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elearning.Application.Repositories
{
    public interface IOrderWriteOnlyRepository
    {
        Task Add(Order order, List<OrderTestExam> orderTestExams);
        Task Update(Order order);
        Task Delete(Order order);
    }
}

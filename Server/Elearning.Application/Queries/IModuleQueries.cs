﻿using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
   public interface IModuleQueries
    {
        Task<ModuleViewModel> GetModule(Guid id);
        Task<List<ModuleViewModel>> GetListModule();
    }
}

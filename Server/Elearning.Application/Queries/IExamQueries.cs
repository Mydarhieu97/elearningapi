﻿using Elearning.Application.ViewModels;
using Elearning.Domain.Extended;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
    public interface IExamQueries
    {
        Task<ExamViewModel> GetExam(Guid id);
        Task<PagedList<ListExamViewModel>> GetExams(ExamParams examParams);
    }
}

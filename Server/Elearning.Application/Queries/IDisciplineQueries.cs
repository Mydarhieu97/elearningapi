﻿using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
   public interface IDisciplineQueries
    {
       Task<DisciplineViewModel> GetDiscipline(Guid id);
       Task<List<DisciplineViewModel>>  GetListDiscipline();
       Task<IList<DisciplineViewModel>> GetDisciplineByParent(Guid parentId);
    }
}

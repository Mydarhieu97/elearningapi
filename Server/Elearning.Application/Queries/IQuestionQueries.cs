﻿using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
    public interface IQuestionQueries
    {
        Task<QuestionViewModel> GetQuestion(Guid id);
        Task<List<QuestionViewModel>> GetQuestions();
    }
}

﻿using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
    public interface IRespondQueries
    {
        Task<RespondViewModel> GetRespond(Guid id);
        Task<List<RespondViewModel>> GetResponds();
    }
}

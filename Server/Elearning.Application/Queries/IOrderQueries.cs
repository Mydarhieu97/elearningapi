﻿using Elearning.Application.Commands.Order;
using Elearning.Application.Results;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elearning.Application.Queries
{
    public interface IOrderQueries
    {
        Task<OrderResult> Get(Guid orderId);
        Task<List<OrderResult>> GetAll();
        Task<List<OrderResult>> GetByUserId(Guid userId);
        Task<List<OrderTestExamResult>> GetOrderTestExamResult(List<Guid> orderTestExamIds);
        Task<string> GetTestExamName(Guid testExamId);
    }
}

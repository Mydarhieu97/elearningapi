﻿using Elearning.Domain.Questions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.ViewModels
{
    public class QuestionViewModel
    {
        public Guid QuestionID { get; set; }
        public string QuestionContent { get; set; }
        public string MediaUrl { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid PersonCreated { get; set; }
        public bool IsLock { get; set; }
        public Guid QuestionTypeID { get; set; }
        public List<RespondViewModel> Responds { get; }

        public QuestionViewModel(
            Guid questionID,
            string questionContent,
            string mediaUrl,
            DateTime createdDate,
            Guid personCreated,
            bool isLock,
            Guid questionTypeID,

            List<RespondViewModel> responds)
        {
            QuestionID = questionID;
            QuestionContent = questionContent;
            MediaUrl = mediaUrl;
            CreatedDate = createdDate;
            PersonCreated = personCreated;
            IsLock = isLock;
            QuestionTypeID = questionTypeID;
            Responds = responds;

        }
    }
}

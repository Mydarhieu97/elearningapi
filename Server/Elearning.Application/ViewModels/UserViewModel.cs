﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.ViewModels
{
    public sealed class UserViewModel
    {
        public Guid Id { get; }
        public string Name { get; }
        public string PhoneNumber { get; }

        public UserViewModel(
            Guid id,
            string name,
            string phoneNumber)
        {
            Id = id;
            PhoneNumber = phoneNumber;
            Name = name;
        }
    }
}

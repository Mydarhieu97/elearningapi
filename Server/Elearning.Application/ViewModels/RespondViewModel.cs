﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.ViewModels
{
    public class RespondViewModel
    {
        public Guid RespondID { get; set; }
        public string RespondContent { get; set; }
        public bool IsTrue { get; set; }
        public Guid QuestionID { get; set; }
        public string MediaUrl { get; set; }
        public int RespondStatus { get; set; }

        public RespondViewModel(
            Guid respondID,
            string respondContent,
            bool isTrue,
            Guid questionID,
            string mediaUrl,
            int respondStatus)
        {
            RespondID = respondID;
            RespondContent = respondContent;
            IsTrue = isTrue;
            QuestionID = questionID;
            MediaUrl = mediaUrl;
            RespondStatus = respondStatus;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.ViewModels
{
   public class ModuleViewModel
    {
        public Guid ModuleID { get; }
        public string ModuleName { get; }
        public int ModuleStatus { get; }

        public ModuleViewModel(
           Guid id,
           string name,
           int status)
        {
            ModuleID = id;
            ModuleName = name;
            ModuleStatus = status;
        }
    }
}

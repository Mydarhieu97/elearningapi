﻿using System;
using System.Collections.Generic;
using System.Text;
using static Elearning.Domain.ElearningEnum;

namespace Elearning.Application.ViewModels
{
    public sealed class ExamViewModel
    {
        public Guid TestExamID { get; set; }

        public string TestExamName { get; set; }
        public ExamStatus TestExamStatus { get; set; }
        public Guid DisciplineID { get; set; }
        public Guid ModuleID { get; set; }
        public List<QuestionViewModel> Questions { get; }

        public ExamViewModel(
            Guid testExamID,
            string testExamName,
            ExamStatus testExamStatus,
            Guid disciplineID,
            Guid moduleID,
            List<QuestionViewModel> questions)
        {
            TestExamID = testExamID;
            TestExamName = testExamName;
            TestExamStatus = testExamStatus;
            DisciplineID = disciplineID;
            ModuleID = moduleID;
            Questions = questions;
        }
    }
}

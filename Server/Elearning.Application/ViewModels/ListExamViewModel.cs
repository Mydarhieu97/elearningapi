﻿using System;
using System.Collections.Generic;
using System.Text;
using static Elearning.Domain.ElearningEnum;

namespace Elearning.Application.ViewModels
{
    public class ListExamViewModel
    {
        
        public Guid TestExamID { get; set; }
        public string TestExamName { get; set; }
        public ExamStatus TestExamStatus { get; set; }
        public Guid DisciplineID { get; set; }
        public Guid ModuleID { get; set; }
        

        public ListExamViewModel(
            Guid testExamID,
            string testExamName,
            ExamStatus testExamStatus,
            Guid disciplineID,
            Guid moduleID
           )
            {
            TestExamID = testExamID;
            TestExamName = testExamName;
            TestExamStatus = testExamStatus;
            DisciplineID = disciplineID;
            ModuleID = moduleID;
           
        }
    }
}

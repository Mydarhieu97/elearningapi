﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.ViewModels
{
   public class DisciplineViewModel
    {
        public Guid DisciplineID { get; }
        public string DisciplineName { get; }
        public int DisciplineStatus { get;  }
        public string DisciplineCode { get; set; }
        public string Description { get; set; }
        public Guid? ParentID { get; set; }

        public DisciplineViewModel() { }
        public DisciplineViewModel(
           Guid id,
           string name,
           int status, string code,string description,
           Guid? parentId)
        {
            DisciplineID = id;
            DisciplineName = name;
            DisciplineStatus = status;
            DisciplineCode = code;
            Description = description;
            ParentID = parentId;
        }
    }
}

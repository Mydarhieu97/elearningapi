﻿using Elearning.Application.Repositories;
using Elearning.Domain.ModuleDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Commands.Module
{
    public class ModuleUseCase : IModuleUseCase
    {
        private readonly IModuleWriteOnlyRepository _moduleWriteOnlyRepository;
        private readonly IModuleReadOnlyRepository _moduleReadOnlyRepository;
       
        public ModuleUseCase(IModuleWriteOnlyRepository moduleWriteOnlyRepository,
            IModuleReadOnlyRepository moduleReadOnlyRepository)
        {
            _moduleWriteOnlyRepository = moduleWriteOnlyRepository;
            _moduleReadOnlyRepository = moduleReadOnlyRepository;
        }
        public async Task<ModuleDTO> Execute(string name)
        {
            ModuleDomain moduleDomain = new ModuleDomain(name);
            await _moduleWriteOnlyRepository.Add(moduleDomain);
            ModuleDTO result = new ModuleDTO(moduleDomain);
            return result;
        }
    }
}

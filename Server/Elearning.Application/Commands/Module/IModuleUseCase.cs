﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Commands.Module
{
   public interface IModuleUseCase
    {
        Task<ModuleDTO> Execute(string name);
    }
}

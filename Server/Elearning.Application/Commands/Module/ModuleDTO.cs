﻿using Elearning.Application.ViewModels;
using Elearning.Domain.ModuleDomain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.Commands.Module
{
   public class ModuleDTO
    {
        public ModuleViewModel moduleViewModel { get; }
        public ModuleDTO(ModuleDomain moduleDomain)
        {
            moduleViewModel = new ModuleViewModel(moduleDomain.Id, moduleDomain.Name, moduleDomain.Status);
        }
    }
}

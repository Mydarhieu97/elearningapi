﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Application.Commands.Discipline
{
   public interface IDisciplineUseCase
    {
        Task<DisciplineDTO> Execute(string name, string code, string desc, string parentID);
      //  Task<DisciplineDTO> ExcuteForUpdate(DisciplineForUpdate disciplineForUpdate);

    }
}

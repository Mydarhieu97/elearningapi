﻿using Elearning.Application.ViewModels;
using Elearning.Domain.DisciplineDomain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Application.Commands.Discipline
{
   public sealed class DisciplineDTO
    {
        public DisciplineViewModel displineViewModel { get; }
        public DisciplineDTO(DisiplineDomain discipline)
        {
            displineViewModel = new DisciplineViewModel(discipline.Id, discipline.Name, discipline.Status,
                discipline.Code, discipline.Description,discipline.ParentID);
        }
    }
}

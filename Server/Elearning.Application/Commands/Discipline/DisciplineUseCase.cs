﻿using System;
using System.Collections.Generic;
using System.Text;
using Elearning.Domain.DisciplineDomain;
using System.Threading.Tasks;
using Elearning.Application.Repositories;

namespace Elearning.Application.Commands.Discipline
{
   public class DisciplineUseCase : IDisciplineUseCase
    {
        private readonly IDisciplineWriteOnlyRepository _disciplineWriteOnlyRepository;
        private readonly IDisciplineReadOnlyRepository _disciplineReadOnlyRepository;

        public DisciplineUseCase(IDisciplineWriteOnlyRepository idisciplineWriteOnlyRepository, IDisciplineReadOnlyRepository disciplineReadOnlyRepository)
        {
            _disciplineWriteOnlyRepository = idisciplineWriteOnlyRepository;
            _disciplineReadOnlyRepository = disciplineReadOnlyRepository;
        }
        public async Task<DisciplineDTO> Execute(string name, string code, string desc, string parentID)
        {
            DisiplineDomain disciplineDomain = new DisiplineDomain(name, code, desc, parentID);
            await _disciplineWriteOnlyRepository.Add(disciplineDomain);
            DisciplineDTO result = new DisciplineDTO(disciplineDomain);
            return result;
        }
    }
}

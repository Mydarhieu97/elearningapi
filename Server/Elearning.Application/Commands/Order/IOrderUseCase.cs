﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Elearning.Application.Commands.Order
{
    public interface IOrderUseCase
    {
        Task<OrderResult> Execute(string userId, List<string> testExamIds);
    }
}

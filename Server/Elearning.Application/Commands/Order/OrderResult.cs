﻿using Elearning.Application.Results;
using System;
using System.Collections.Generic;

namespace Elearning.Application.Commands.Order
{
    public class OrderResult
    {
        public OrderResult
            (Guid id,
            string username,
            DateTime orderTime,
            string orderStatus,
            IList<OrderTestExamResult> orderTestExams)
        {
            Id = id;
            Username = username;
            OrderTime = orderTime;
            OrderStatus = orderStatus;
            OrderTestExams = orderTestExams;
        }

        public OrderResult(Domain.Orders.Order order,
            IList<OrderTestExamResult> orderTestExams)
        {
            Id = order.Id;
            Username = order.Username;
            OrderTime = order.OrderTime;
            OrderStatus = order.OrderStatus.ToString();
            OrderTestExams = orderTestExams;
        }

        public Guid Id { get; }
        public string Username { get; }
        public DateTime OrderTime { get; }
        public string OrderStatus { get; }
        public IList<OrderTestExamResult> OrderTestExams { get; }
    }
}

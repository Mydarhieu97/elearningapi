﻿using Elearning.Application.Queries;
using Elearning.Application.Repositories;
using Elearning.Domain.Orders;
using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Application.Commands.Order
{
    public class OrderUseCase : IOrderUseCase
    {
        private readonly IOrderQueries _orderQueries;
        private readonly IOrderWriteOnlyRepository _orderWriteOnlyRepository;
        private readonly IOrderReadOnlyRepository _orderReadOnlyRepository;

        public OrderUseCase(IOrderQueries orderQueries,
            IOrderWriteOnlyRepository orderWriteOnlyRepository,
            IOrderReadOnlyRepository orderReadOnlyRepository)
        {
            _orderQueries = orderQueries;
            _orderWriteOnlyRepository = orderWriteOnlyRepository;
            _orderReadOnlyRepository = orderReadOnlyRepository;
        }

        public async Task<OrderResult> Execute(string userId, List<string> testExamIds)
        {
            var order = new Domain.Orders.Order(Guid.Parse(userId), new OrderStatus(1));

            var orderTestExams = new List<OrderTestExam>();
            foreach (var testExamId in testExamIds)
            {
                var testExamName = await _orderQueries.GetTestExamName(Guid.Parse(testExamId));

                var orderTestExam = new OrderTestExam(order.Id, Guid.Parse(testExamId),
                    new OrderTestExamStatus(1), testExamName);

                orderTestExams.Add(orderTestExam);
            }

            await _orderWriteOnlyRepository.Add(order, orderTestExams);

            var orderTestExamIds = orderTestExams.Select(ote => ote.Id).ToList();
            var orderTestExamResults = await _orderQueries.GetOrderTestExamResult(orderTestExamIds);

            var orderInDb = await _orderReadOnlyRepository.Get(order.Id);


            var orderResult = new OrderResult(orderInDb.Id, orderInDb.Username,
                orderInDb.OrderTime, orderInDb.OrderStatus.ToString(), orderTestExamResults);

            return orderResult;
        }
    }
}

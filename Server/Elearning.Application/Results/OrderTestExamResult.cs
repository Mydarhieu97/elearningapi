﻿using Elearning.Domain.Orders;
using System;

namespace Elearning.Application.Results
{
    public class OrderTestExamResult
    {
        public OrderTestExamResult(Guid id, string testExamName, string orderTestExamStatus)
        {
            Id = id;
            TestExamName = testExamName;
            OrderTestExamStatus = orderTestExamStatus;
        }

        public OrderTestExamResult(OrderTestExam orderTestExam)
        {
            Id = orderTestExam.Id;
            TestExamName = orderTestExam.TestExamName;
            OrderTestExamStatus = orderTestExam.Status.ToString();
        }
        public Guid Id { get; }
        public string TestExamName { get; }
        public string OrderTestExamStatus { get; }
    }
}

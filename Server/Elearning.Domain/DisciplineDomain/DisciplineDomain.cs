﻿using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain.DisciplineDomain
{
    public class DisiplineDomain: IEntity, IAggregateRoot
    {
        public Guid Id { get; set; }
        public Name Name { get; set; }
        public int Status { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public Guid? ParentID { get; set; }
        public DisiplineDomain(Name name, Name code, Name desc, Name parentID)
        {
            Id = Guid.NewGuid();
            Name = name;
            Code = code;
            Description = desc;
            if (parentID == "2f3d84d2-94f3-478c-8558-77d92fcf89cc")
            {
                ParentID = null;
            }
            else
            {
                ParentID = Guid.Parse(parentID);
            }
            Status = 1;
        }
        public DisiplineDomain() { }
        public static DisiplineDomain Load(Guid id, Name name, Name code, int status,string desc, Guid parentID)
        {
            DisiplineDomain oneDiscipline = new DisiplineDomain();
            oneDiscipline.Id = id;
            oneDiscipline.Name = name;
            oneDiscipline.Code = code;
            oneDiscipline.Status = status;
            oneDiscipline.Description = desc;
            oneDiscipline.ParentID = parentID;
            return oneDiscipline;
        }
    }
   
}

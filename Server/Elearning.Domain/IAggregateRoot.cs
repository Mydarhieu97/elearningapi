﻿using System;

namespace Elearning.Domain
{
    internal interface IAggregateRoot : IEntity
    {
    }
}

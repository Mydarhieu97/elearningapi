﻿using System;
using System.Runtime.Serialization;

namespace Elearning.Domain.ValueObjects
{
    [Serializable]
    internal sealed class InvalidPhoneNumberException : DomainException
    {
        public InvalidPhoneNumberException(string message) : base(message)
        {
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Elearning.Domain.ValueObjects
{
    public sealed class PhoneNumber
    {
        private string _text { get; set; }

        const string PN1 = @"^[0]{1}[3]{1}[0-9]{8}$";

        const string PN2 = @"^\+[0-9]{2}\s+[0-9]{2}\s+[0-9]{8}$";

        const string PN3 = @"^[0-9]{3}-[0-9]{3}-[0-9]{4}$";


        public PhoneNumber(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                throw new PhoneNumberShouldNotBeEmptyException("The 'Phone Number' field is required");

            Regex regexType1 = new Regex(PN1);

            Regex regexType2 = new Regex(PN2);

            Regex regexType3 = new Regex(PN3);

            Match match1 = regexType1.Match(text);

            Match match2 = regexType2.Match(text);

            Match match3 = regexType3.Match(text);

            if (!match1.Success)
                throw new InvalidPhoneNumberException("Invalid Phone Number format. Use 03xxxxxxxx or 09xxxxxxxx.");

            this._text = text;
        }


        public override string ToString()
        {
            return _text.ToString();
        }

        public static implicit operator PhoneNumber(string text)
        {
            return new PhoneNumber(text);
        }

        public static implicit operator string(PhoneNumber name)
        {
            return name._text;
        }



        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj is string)
            {
                return obj.ToString() == _text;
            }

            return ((PhoneNumber)obj)._text == _text;
        }

        public override int GetHashCode()
        {
            return _text.GetHashCode();
        }

    }
}

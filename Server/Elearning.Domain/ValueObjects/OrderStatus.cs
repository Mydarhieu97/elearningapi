﻿using System;

namespace Elearning.Domain.ValueObjects
{
    public class OrderStatus
    {
        private readonly int _value;

        private enum Status
        {
            Pending,
            Completed,
            Error,
            Deleted = 99
        }

        public OrderStatus(int value)
        {
            if (!Enum.IsDefined(typeof(Status), value))
            {
                throw new Exception("Status is invalid");
            }

            _value = value;
        }

        public int GetValue()
        {
            return _value;
        }

        public override string ToString()
        {
            return Enum.GetName(typeof(Status), _value);
        }

    }
}

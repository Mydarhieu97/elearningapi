﻿using System;
using System.Runtime.Serialization;

namespace Elearning.Domain.ValueObjects
{
    [Serializable]
    internal sealed class NameShouldNotBeEmptyException : DomainException
    {
        public NameShouldNotBeEmptyException(string message) : base(message)
        {
        }

    }
}
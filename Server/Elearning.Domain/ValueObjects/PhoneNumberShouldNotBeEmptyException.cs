﻿using System;
using System.Runtime.Serialization;

namespace Elearning.Domain.ValueObjects
{
    [Serializable]
    internal sealed class PhoneNumberShouldNotBeEmptyException : DomainException
    {
        public PhoneNumberShouldNotBeEmptyException(string message) : base(message)
        {
        }

    }
}
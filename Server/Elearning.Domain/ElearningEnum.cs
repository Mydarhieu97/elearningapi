﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain
{
    public class ElearningEnum
    {
        public enum ExamStatus
        {
            Inactive = 0,

            JustCreated = 1,

            AwaitingApproval = 2,

            Approved = 3,

            CanceledApproval = 4

        }
        public enum DisciplineStatus
        {
            InActive = 0,
            Active = 1
        }
        public enum ModuleStatus
        {
            Inactive = 0,
            Active = 1
        }

        public enum OrderStatus
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain
{
    internal interface IEntity
    {
        Guid Id { get; }
    }
}

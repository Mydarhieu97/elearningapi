﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Elearning.Domain.TestExams
{
    public class QuestionCollection
    {
        private readonly IList<Guid> _testExamQuestionIds;

        public QuestionCollection()
        {
            _testExamQuestionIds = new List<Guid>();
        }

        public IReadOnlyCollection<Guid> GetQuestionIds()
        {
            IReadOnlyCollection<Guid> testExamQuestionIds = new ReadOnlyCollection<Guid>(_testExamQuestionIds);
            return testExamQuestionIds;
        }

        public void Add(Guid testExamQuestionId)
        {
            _testExamQuestionIds.Add(testExamQuestionId);
        }
    }
}

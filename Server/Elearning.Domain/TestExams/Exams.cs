﻿using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;
using static Elearning.Domain.ElearningEnum;

namespace Elearning.Domain.TestExams
{
    public sealed class Exams : IEntity, IAggregateRoot
    {
        public Guid Id { get; set; }

        public Name TestExamName { get; set; }
        public ExamStatus TestExamStatus { get; set; }
        public Guid DisciplineID { get; set; }
        public Guid ModuleID { get; set; }

        public IReadOnlyCollection<Guid> Questions
        {
            get
            {
                IReadOnlyCollection<Guid> readOnly = _questions.GetQuestionIds();
                return readOnly;
            }
        }

        private QuestionCollection _questions;

        public Exams(Name testExamName, ExamStatus testExamStatus, Guid disciplineID, Guid moduleID)
        {
            Id = Guid.NewGuid();
            TestExamName = testExamName;
            TestExamStatus = testExamStatus;
            DisciplineID = disciplineID;
            ModuleID = moduleID;
            _questions = new QuestionCollection();
        }

        private Exams() { }

        public static Exams Load(Guid id, Name testExamName,
            ExamStatus testExamStatus,
            Guid disciplineID,
            Guid moduleID,
            QuestionCollection questions)
        {
            Exams exam = new Exams();
            exam.Id = id;
            exam.TestExamName = testExamName;
            exam.TestExamStatus = testExamStatus;
            exam.DisciplineID = disciplineID;
            exam.ModuleID = moduleID;
            exam._questions = questions;
            return exam;
        }
    }
}

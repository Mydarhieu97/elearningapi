﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain.Extended
{
    public class ExamParams
    {
        private const int MaxPageSize = 50;
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;

        public int MyProperty
        {
            get { return PageSize; }
            set { PageSize = (value > MaxPageSize) ? MaxPageSize : value; }
        }

        public string DisciplineID { get; set; }
        public string ModuleID { get; set; }

    }
}

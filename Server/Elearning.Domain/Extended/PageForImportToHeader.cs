﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain.Extended
{
    public class PageForImportToHeader
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int PageSize { get; set; }
        public int TotalCount { get; set; }
        public PageForImportToHeader(int currentPage, int totalPages, int pageSize, int TotalCount)
        {
            CurrentPage = currentPage;
            TotalPages = totalPages;
            PageSize = pageSize;
            TotalPages = totalPages;
        }

        public PageForImportToHeader()
        {
        }
    }
}

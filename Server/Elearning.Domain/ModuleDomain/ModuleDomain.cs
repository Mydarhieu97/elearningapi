﻿using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain.ModuleDomain
{
   public class ModuleDomain: IEntity, IAggregateRoot
    {
        public Guid Id { get; set; }
        public Name Name { get; set; }
        public int Status { get; set; }
        public ModuleDomain(Name name)
        {
            Id = Guid.NewGuid();
            Name = name;
            Status = 1;
        }
        public ModuleDomain() { }
        public static ModuleDomain Load(Guid id, Name name, int status)
        {
            ModuleDomain oneModule = new ModuleDomain();
            oneModule.Id = id;
            oneModule.Name = name;
            oneModule.Status = status;
            return oneModule;
        }
    }
}

﻿using Elearning.Domain.ValueObjects;
using System;

namespace Elearning.Domain.Orders
{
    public class OrderTestExam
    {
        public Guid Id { get; private set; }
        public Guid OrderId { get; private set; }
        public Guid TestExamId { get; private set; }
        public OrderTestExamStatus Status { get; private set; }
        public string TestExamName { get; private set; }
        private OrderTestExam() { }
        public OrderTestExam(Guid orderId, Guid testExamId, OrderTestExamStatus status, string testExamName)
        {
            Id = Guid.NewGuid();
            OrderId = orderId;
            TestExamId = testExamId;
            Status = status;
            TestExamName = testExamName;
        }
        public static OrderTestExam Load(Guid id, Guid orderId, Guid testExamId, OrderTestExamStatus status, string testExamName)
        {
            var orderTestExam = new OrderTestExam()
            {
                Id = id,
                OrderId = orderId,
                TestExamId = testExamId,
                Status = status,
                TestExamName = testExamName
            };

            return orderTestExam;
        }
    }
}

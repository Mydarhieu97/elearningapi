﻿using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;

namespace Elearning.Domain.Orders
{
    public class Order
    {
        public Guid Id { get; private set; }
        public Guid UserId { get; private set; }
        public DateTime OrderTime { get; private set; }
        public OrderStatus OrderStatus { get; private set; }
        public string Username { get; private set; }
        public IReadOnlyCollection<Guid> OrderTestExams
        {
            get
            {
                var orderTestExam = _orderTestExamCollection.GetOrderTestExamIds();
                return orderTestExam;
            }
        }

        private OrderTestExamCollection _orderTestExamCollection;
        private Order() { }
        public Order(Guid userId, OrderStatus orderStatus)
        {
            Id = Guid.NewGuid();
            UserId = userId;
            OrderStatus = orderStatus;
            _orderTestExamCollection = new OrderTestExamCollection();
        }
        public static Order Load(Guid id, Guid userId, OrderStatus orderStatus, string username,
            OrderTestExamCollection orderTestExamCollection)
        {
            var order = new Order()
            {
                Id = id,
                UserId = userId,
                OrderTime = DateTime.Now,
                OrderStatus = orderStatus,
                Username = username,
                _orderTestExamCollection = orderTestExamCollection
            };

            return order;
        }
    }
}

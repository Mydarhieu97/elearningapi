﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Elearning.Domain.Orders
{
    public sealed class OrderTestExamCollection
    {
        private readonly IList<Guid> _orderTestExamIds;

        public OrderTestExamCollection()
        {
            _orderTestExamIds = new List<Guid>();
        }

        public IReadOnlyCollection<Guid> GetOrderTestExamIds()
        {
            var orderTestExamIds = new ReadOnlyCollection<Guid>(_orderTestExamIds);
            return orderTestExamIds;
        }

        public void Add(Guid orderTestExamId)
        {
            _orderTestExamIds.Add(orderTestExamId);
        }
        public void Add(IEnumerable<Guid> orderTestExamIds)
        {
            foreach (var orderTestExamId in orderTestExamIds)
            {
                _orderTestExamIds.Add(orderTestExamId);
            }
        }
    }
}

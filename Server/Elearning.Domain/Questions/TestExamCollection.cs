﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Elearning.Domain.Questions
{
    public class TestExamCollection
    {
        private readonly IList<Guid> _testExamQuestionIds;

        public TestExamCollection()
        {
            _testExamQuestionIds = new List<Guid>();
        }

        public IReadOnlyCollection<Guid> GetExamIds()
        {
            IReadOnlyCollection<Guid> testExamQuestionIds = new ReadOnlyCollection<Guid>(_testExamQuestionIds);
            return testExamQuestionIds;
        }

        public void Add(Guid testExamQuestionId)
        {
            _testExamQuestionIds.Add(testExamQuestionId);
        }
    }
}

﻿using Elearning.Domain.ValueObjects;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Domain.Questions
{
    public sealed class Questions : IEntity, IAggregateRoot
    {
        public Guid Id { get; set; }
        public Guid TestExamID { get; set; }
        public Name QuestionContent { get; set; }

        public IReadOnlyCollection<Guid> Responds
        {
            get
            {
                IReadOnlyCollection<Guid> readOnly = _responds.GetRespondIds();
                return readOnly;
            }
        }

        private RespondCollection _responds;

        public IReadOnlyCollection<Guid> Exams
        {
            get
            {
                IReadOnlyCollection<Guid> readOnly = _exams.GetExamIds();
                return readOnly;
            }
        }

        private TestExamCollection _exams;

        public Questions(Guid testExamID, Name questionContent)
        {
            Id = Guid.NewGuid();
            TestExamID = testExamID;
            QuestionContent = questionContent;
            _exams = new TestExamCollection();
            _responds = new RespondCollection();
        }

        private Questions() { }

        public static Questions Load(Guid id,
            Guid testExamID,
            Name questionContent,
            TestExamCollection exams,
            RespondCollection responds)
        {
            Questions question = new Questions();
            question.Id = id;
            question.TestExamID = testExamID;
            question.QuestionContent = questionContent;
            question._exams = exams;
            question._responds = responds;
            return question;
        }
    }
}

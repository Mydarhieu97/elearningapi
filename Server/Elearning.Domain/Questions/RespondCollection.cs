﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Elearning.Domain.Questions
{
    public sealed class RespondCollection
    {
        private readonly IList<Guid> _respondIds;

        public RespondCollection()
        {
            _respondIds = new List<Guid>();
        }

        public IReadOnlyCollection<Guid> GetRespondIds()
        {
            IReadOnlyCollection<Guid> respondIds = new ReadOnlyCollection<Guid>(_respondIds);
            return respondIds;
        }

        public void Add(Guid respondId)
        {
            _respondIds.Add(respondId);
        }
    }
}

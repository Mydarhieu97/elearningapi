﻿using Elearning.Application.Repositories;
using Elearning.Domain.DisciplineDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Repositories
{
    public class DisciplineRepositories : IDisciplineWriteOnlyRepository, IDisciplineReadOnlyRepository
    {
        private readonly ElearningContext _context;
        public DisciplineRepositories(ElearningContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task Add(DisiplineDomain discipline)
        {
            Entities.Discipline disciplineEntity = new Entities.Discipline()
            {
                DisciplineID = discipline.Id,
                Code=discipline.Code,
                ParentId=discipline.ParentID,
                Description=discipline.Description,
                DisciplineName = discipline.Name,
                DisciplineStatus = discipline.Status
            };
            await _context.Disciplines.AddAsync(disciplineEntity);
            await _context.SaveChangesAsync();
        }
        public Task Delete(Guid id)
        {
            Entities.Discipline oneDiscipline = _context.Disciplines.Find(id);
            if (oneDiscipline!=null)
            {
                _context.Disciplines.Remove(oneDiscipline);
                _context.SaveChanges();
            }
            return Task.CompletedTask;
        }

        public List<DisiplineDomain> GetListDiscipline()
        {
            throw new NotImplementedException();
        }

        public Task<DisiplineDomain> GetOneDiscipline(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task Update(Guid disciplineIDUpdate, string disciplineNameUpdate,
            int disciplineStatusUpdate, string disciplineCodeUpdate)
        {
            if (disciplineIDUpdate!=null)
            {
                Entities.Discipline oneDiscipline = _context.Disciplines.Find(disciplineIDUpdate);
                oneDiscipline.DisciplineName = disciplineNameUpdate;
                oneDiscipline.DisciplineStatus = disciplineStatusUpdate;
               // oneDiscipline.DisciplineCode = disciplineCodeUpdate;
                _context.Disciplines.Update(oneDiscipline);
                _context.SaveChanges();
                return Task.CompletedTask;
            }
            return Task.CompletedTask;
        }
    }
}

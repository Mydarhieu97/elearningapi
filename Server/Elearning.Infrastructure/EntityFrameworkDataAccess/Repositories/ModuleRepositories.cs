﻿using Elearning.Application.Repositories;
using Elearning.Application.ViewModels;
using Elearning.Domain.ModuleDomain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Repositories
{
    public class ModuleRepositories : IModuleReadOnlyRepository, IModuleWriteOnlyRepository
    {
        private readonly ElearningContext _context;
        public ModuleRepositories(ElearningContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        public async Task Add(ModuleDomain moduleDomain)
        {
            Entities.Module moduleEntity = new Entities.Module()
            {
                ModuleID = moduleDomain.Id,
                ModuleName = moduleDomain.Name,
                ModuleStatus = moduleDomain.Status
            };
            await _context.Modules.AddAsync(moduleEntity);
            await _context.SaveChangesAsync();
        }

        public Task Delete(Guid id)
        {
            Entities.Module moduleEntity = _context.Modules.Find(id);
            _context.Remove(moduleEntity);
            _context.SaveChanges();
            return Task.CompletedTask;
        }

        public List<ModuleDomain> GetListModule()
        {
            throw new NotImplementedException();
        }

        public Task<ModuleDomain> GetOneModule(Guid id)
        {
            throw new NotImplementedException();
        }
        public Task Update(Guid idModule, string moduleName)
        {
            if (idModule != null && moduleName!=null)
            {
                Entities.Module module = _context.Modules.Find(idModule);
                module.ModuleName = moduleName;
                _context.Modules.Update(module);
                _context.SaveChanges();
                return Task.CompletedTask;
            }
            return Task.CompletedTask;
        }
    }
}

﻿using Elearning.Application.Repositories;
using Elearning.Domain.Orders;
using Elearning.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Repositories
{
    public class OrderRepository : IOrderReadOnlyRepository, IOrderWriteOnlyRepository
    {
        private readonly ElearningContext _context;

        public OrderRepository(ElearningContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Order> Get(Guid orderId)
        {
            var orderInDb = await _context.Orders
                                        .Include(o => o.User)
                                        .FirstOrDefaultAsync(o => o.OrderId == orderId)
                                        .ConfigureAwait(false);

            if (orderInDb == null) throw new NullReferenceException();

            var orderTestExamsInDb = await _context.OrderTestExams
                .Where(ote => ote.OrderId == orderId)
                .Select(ote => ote.OrderTestExamId)
                .ToListAsync()
                .ConfigureAwait(false);

            var orderTestExamCollection = new OrderTestExamCollection();
            foreach (var orderTestExam in orderTestExamsInDb)
            {
                orderTestExamCollection.Add(orderTestExam);
            }

            var orderStatus = new OrderStatus(orderInDb.OrderStatus);

            return Order.Load(orderInDb.OrderId,
                orderInDb.UserId, orderStatus, orderInDb.User.UserName, orderTestExamCollection);
        }

        public async Task Add(Order order, List<OrderTestExam> orderTestExams)
        {
            if (order == null || orderTestExams == null) throw new NullReferenceException();

            var orderToSave = new Entities.Order()
            {
                OrderId = order.Id,
                UserId = order.UserId,
                OrderStatus = order.OrderStatus.GetValue(),
                OrderTime = order.OrderTime
            };

            await _context.Orders.AddAsync(orderToSave);
            await _context.SaveChangesAsync().ConfigureAwait(false);

            foreach (var orderTestExam in orderTestExams)
            {
                var orderTestExamToSave = new Entities.OrderTestExam()
                {
                    OrderTestExamId = orderTestExam.Id,
                    OrderId = orderTestExam.OrderId,
                    TestExamId = orderTestExam.TestExamId,
                    OrderTestExamStatus = orderTestExam.Status.GetValue()
                };

                await _context.OrderTestExams.AddAsync(orderTestExamToSave);
                await _context.SaveChangesAsync().ConfigureAwait(false);
            }
        }

        public async Task Update(Order order)
        {
            if (order == null) throw new ArgumentNullException(nameof(order));

            var orderToSave = new Entities.Order
            {
                OrderId = order.Id,
                OrderStatus = order.OrderStatus.GetValue(),
                OrderTime = order.OrderTime,
                UserId = order.UserId
            };

            _context.Orders.Update(orderToSave);
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task Delete(Order order)
        {
            if (order == null) throw new ArgumentNullException(nameof(order));

            var orderToSave = new Entities.Order
            {
                OrderId = order.Id,
                OrderStatus = 99,
                OrderTime = order.OrderTime,
                UserId = order.UserId
            };

            _context.Orders.Update(orderToSave);
            await _context.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}

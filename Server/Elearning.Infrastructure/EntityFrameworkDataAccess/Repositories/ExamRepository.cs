﻿using Elearning.Application.Repositories;
using Elearning.Domain.TestExams;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Repositories
{
    public class ExamRepository : IExamReadOnlyRepository, IExamWriteOnlyRepository
    {
        public Task Add(Exams exam)
        {
            throw new NotImplementedException();
        }

        public Task Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<Exams> GetExam(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<Exams> GetExams()
        {
            throw new NotImplementedException();
        }

        public Task Update(Exams exam)
        {
            throw new NotImplementedException();
        }
    }
}

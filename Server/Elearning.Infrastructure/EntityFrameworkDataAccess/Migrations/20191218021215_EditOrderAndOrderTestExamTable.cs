﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elearning.Infrastructure.Migrations
{
    public partial class EditOrderAndOrderTestExamTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderTestExams_Orders_OrderID",
                table: "OrderTestExams");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderTestExams_TestExams_TestExamID",
                table: "OrderTestExams");

            migrationBuilder.DropColumn(
                name: "QuantityTestExam",
                table: "OrderTestExams");

            migrationBuilder.DropColumn(
                name: "TotalPayment",
                table: "Orders");

            migrationBuilder.RenameColumn(
                name: "TestExamID",
                table: "OrderTestExams",
                newName: "TestExamId");

            migrationBuilder.RenameColumn(
                name: "OrderID",
                table: "OrderTestExams",
                newName: "OrderId");

            migrationBuilder.RenameColumn(
                name: "OrderTestExamID",
                table: "OrderTestExams",
                newName: "OrderTestExamId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderTestExams_TestExamID",
                table: "OrderTestExams",
                newName: "IX_OrderTestExams_TestExamId");

            migrationBuilder.RenameIndex(
                name: "IX_OrderTestExams_OrderID",
                table: "OrderTestExams",
                newName: "IX_OrderTestExams_OrderId");

            migrationBuilder.RenameColumn(
                name: "OrderID",
                table: "Orders",
                newName: "OrderId");

            migrationBuilder.AddColumn<int>(
                name: "OrderTestExamStatus",
                table: "OrderTestExams",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uniqueidentifier",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderTestExams_Orders_OrderId",
                table: "OrderTestExams",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "OrderId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderTestExams_TestExams_TestExamId",
                table: "OrderTestExams",
                column: "TestExamId",
                principalTable: "TestExams",
                principalColumn: "TestExamID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderTestExams_Orders_OrderId",
                table: "OrderTestExams");

            migrationBuilder.DropForeignKey(
                name: "FK_OrderTestExams_TestExams_TestExamId",
                table: "OrderTestExams");

            migrationBuilder.DropColumn(
                name: "OrderTestExamStatus",
                table: "OrderTestExams");

            migrationBuilder.RenameColumn(
                name: "TestExamId",
                table: "OrderTestExams",
                newName: "TestExamID");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "OrderTestExams",
                newName: "OrderID");

            migrationBuilder.RenameColumn(
                name: "OrderTestExamId",
                table: "OrderTestExams",
                newName: "OrderTestExamID");

            migrationBuilder.RenameIndex(
                name: "IX_OrderTestExams_TestExamId",
                table: "OrderTestExams",
                newName: "IX_OrderTestExams_TestExamID");

            migrationBuilder.RenameIndex(
                name: "IX_OrderTestExams_OrderId",
                table: "OrderTestExams",
                newName: "IX_OrderTestExams_OrderID");

            migrationBuilder.RenameColumn(
                name: "OrderId",
                table: "Orders",
                newName: "OrderID");

            migrationBuilder.AddColumn<int>(
                name: "QuantityTestExam",
                table: "OrderTestExams",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<Guid>(
                name: "UserId",
                table: "Orders",
                type: "uniqueidentifier",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<double>(
                name: "TotalPayment",
                table: "Orders",
                type: "float",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_AspNetUsers_UserId",
                table: "Orders",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderTestExams_Orders_OrderID",
                table: "OrderTestExams",
                column: "OrderID",
                principalTable: "Orders",
                principalColumn: "OrderID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_OrderTestExams_TestExams_TestExamID",
                table: "OrderTestExams",
                column: "TestExamID",
                principalTable: "TestExams",
                principalColumn: "TestExamID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

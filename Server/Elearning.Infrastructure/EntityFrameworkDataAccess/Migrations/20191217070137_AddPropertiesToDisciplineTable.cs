﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Elearning.Infrastructure.Migrations
{
    public partial class AddPropertiesToDisciplineTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Modules",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Disciplines",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Disciplines",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ParentId",
                table: "Disciplines",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Modules");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Disciplines");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Disciplines");

            migrationBuilder.DropColumn(
                name: "ParentId",
                table: "Disciplines");
        }
    }
}

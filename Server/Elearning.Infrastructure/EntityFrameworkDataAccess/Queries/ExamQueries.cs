﻿using AutoMapper;
using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using Elearning.Domain.Extended;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
    public class ExamQueries : IExamQueries
    {
        private readonly ElearningContext _context;
        private readonly IQuestionQueries _questionQueries;
        private readonly IMapper _mapper;
        public ExamQueries(ElearningContext context, IQuestionQueries questionQueries, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _questionQueries = questionQueries;
        }
        public async Task<ExamViewModel> GetExam(Guid id)
        {
            var exam = await _context.TestExams.FindAsync(id);

            if (exam == null)
                throw new InfrastructureException($"The Exam {id} does not exists or is not processed yet.");

            var testExamID = _context.TestExamQuestions.Where(test => test.TestExamID == exam.TestExamID).FirstOrDefault();

            if(testExamID == null)
            {
                throw new Exception($"This Exam does not have any question !!!");
            }

            List<Entities.TestExamQuestion> listQuestionID = await _context
                .TestExamQuestions
                .Where(l => l.TestExamID == testExamID.TestExamID)
                .ToListAsync().ConfigureAwait(true);

            List<Entities.Question> questions = new List<Entities.Question>();

            foreach (var item in listQuestionID)
            {
                Entities.Question temp = new Entities.Question();
                temp.QuestionID = item.QuestionID;
                questions.Add(temp);
            }

            

            List<QuestionViewModel> questionsResult = new List<QuestionViewModel>();

            foreach (Entities.Question question in questions)
            {
                QuestionViewModel questionResult = await _questionQueries.GetQuestion(question.QuestionID).ConfigureAwait(true);
                questionsResult.Add(questionResult);
            }

            ExamViewModel examForReturn = new ExamViewModel(
                exam.TestExamID, 
                exam.TestExamName,
                exam.TestExamStatus, 
                exam.DisciplineID,
                exam.ModuleID,
                questionsResult
                );

            return await Task.FromResult<ExamViewModel>(examForReturn).ConfigureAwait(true);
        }

        public async Task<PagedList<ListExamViewModel>> GetExams(ExamParams examParams)
        {

            IQueryable<Entities.TestExam> exams = _context.TestExams.AsQueryable();

            if (!string.IsNullOrEmpty(examParams.DisciplineID))
                exams = exams.Where(d => d.DisciplineID == Guid.Parse(examParams.DisciplineID));

            if (!string.IsNullOrEmpty(examParams.ModuleID))
                exams = exams.Where(d => d.ModuleID == Guid.Parse(examParams.ModuleID));

            var result = await PagedList<Entities.TestExam>
                .CreateAsync(exams, examParams.PageNumber, examParams.PageSize)
                .ConfigureAwait(true);
            return _mapper.Map<PagedList<ListExamViewModel>>(result);


            /*var exams =await _context.TestExams.ToListAsync();


            List<ListExamViewModel> examsForReturn = new List<ListExamViewModel>();

            foreach (var exam in exams)
            {
                Entities.Discipline discipline = _context.Disciplines
                    .FirstOrDefault(d => d.DisciplineID == exam.DisciplineID);
                Entities.Module module = _context.Modules
                    .FirstOrDefault(m => m.ModuleID == exam.ModuleID);
                ListExamViewModel examTemp = new ListExamViewModel(
                    exam.TestExamID,
                    exam.TestExamName,
                    exam.TestExamStatus,
                    discipline.DisciplineName,
                    module.ModuleName
                );
                examsForReturn.Add(examTemp);
            }


            return examsForReturn;*/
        }
    }
}

﻿using Elearning.Application.Commands.Order;
using Elearning.Application.Queries;
using Elearning.Application.Results;
using Elearning.Domain.Orders;
using Elearning.Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
    public class OrderQueries : IOrderQueries
    {
        private readonly ElearningContext _context;
        public OrderQueries(ElearningContext context)
        {
            _context = context;
        }
        public async Task<OrderResult> Get(Guid orderId)
        {
            if (orderId == Guid.Empty) throw new ArgumentNullException(nameof(orderId));

            var orderInDb = await _context.Orders
                .Include(o => o.User)
                .FirstOrDefaultAsync(o => o.OrderId == orderId)
                .ConfigureAwait(false);

            var orderTestExamIdsInDb = await _context.OrderTestExams
                .Where(ote => ote.OrderId == orderId)
                .Select(ote => ote.OrderTestExamId)
                .ToListAsync()
                .ConfigureAwait(false);

            var orderTestExamCollection = new OrderTestExamCollection();
            orderTestExamCollection.Add(orderTestExamIdsInDb);

            var orderStatus = new OrderStatus(orderInDb.OrderStatus);
            var order = Order.Load(orderInDb.OrderId, orderInDb.UserId,
                orderStatus, orderInDb.User.UserName, orderTestExamCollection);

            var orderTestExamResults = await GetOrderTestExamResult(orderTestExamIdsInDb)
                .ConfigureAwait(false);
            var orderTestExamResultsReadOnly = new ReadOnlyCollection<OrderTestExamResult>(orderTestExamResults);

            var result = new OrderResult(order, orderTestExamResultsReadOnly);

            return result;
        }

        public async Task<List<OrderResult>> GetAll()
        {
            var orderIdsInDb = await _context.Orders
                .Include(o => o.User)
                .AsNoTracking()
                .Select(o => o.OrderId)
                .ToListAsync()
                .ConfigureAwait(false);

            var result = new List<OrderResult>();

            foreach (var id in orderIdsInDb)
            {
                var orderResult = await Get(id).ConfigureAwait(false);
                result.Add(orderResult);
            }

            return result;
        }

        public async Task<List<OrderResult>> GetByUserId(Guid userId)
        {
            if (userId == Guid.Empty) throw new ArgumentNullException(nameof(userId));

            var orderIdsInDb = await _context.Orders
                .Include(o => o.User)
                .Where(o => o.UserId == userId)
                .Select(o => o.OrderId)
                .ToListAsync()
                .ConfigureAwait(false);

            var result = new List<OrderResult>();

            foreach (var id in orderIdsInDb)
            {
                var orderResult = await Get(id).ConfigureAwait(false);
                result.Add(orderResult);
            }

            return result;
        }

        public async Task<List<OrderTestExamResult>> GetOrderTestExamResult(List<Guid> orderTestExamIds)
        {
            if (orderTestExamIds == null) throw new ArgumentNullException(nameof(orderTestExamIds));

            var orderTestExams = await _context.OrderTestExams
                .Include(ote => ote.TestExam)
                .Where(ote => orderTestExamIds.Contains(ote.OrderTestExamId))
                .ToListAsync()
                .ConfigureAwait(false);

            var orderTestExamResults = new List<OrderTestExamResult>();
            foreach (var orderTestExam in orderTestExams)
            {
                var orderTestExamStatus = new OrderTestExamStatus(orderTestExam.OrderTestExamStatus);
                var orderTestExamResult = new OrderTestExamResult(Guid.NewGuid(),
                    orderTestExam.TestExam.TestExamName,
                    orderTestExamStatus.ToString());

                orderTestExamResults.Add(orderTestExamResult);
            }

            return orderTestExamResults;
        }

        public async Task<string> GetTestExamName(Guid testExamId)
        {
            if (testExamId == Guid.Empty) throw new ArgumentNullException(nameof(testExamId));

            var testExam = await _context.TestExams
                .FirstOrDefaultAsync(t => t.TestExamID == testExamId)
                .ConfigureAwait(false);

            return testExam.TestExamName;
        }
    }
}

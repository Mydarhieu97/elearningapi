﻿using AutoMapper;
using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
   public class DisciplineQueries: IDisciplineQueries
    {
        private readonly ElearningContext _context;
        private readonly IMapper _mapper;
        public DisciplineQueries(ElearningContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<DisciplineViewModel> GetDiscipline(Guid id)
        {
            var disci = await _context.Disciplines.FindAsync(id);

            if (disci == null)
                throw new InfrastructureException($"The Discipline {id} does not exists or is not processed yet.");

            DisciplineViewModel disciplineForReturn = new DisciplineViewModel(
               disci.DisciplineID, disci.DisciplineName, disci.DisciplineStatus,
               disci.Code,disci.Description, disci.ParentId
                );
            return await Task.FromResult<DisciplineViewModel>(disciplineForReturn);
        }

        public async Task<List<DisciplineViewModel>> GetListDiscipline()
        {
            var disciplines = _context.Disciplines.AsQueryable();
            List<DisciplineViewModel> listDiscipline = new List<DisciplineViewModel>();
            foreach (var oneDis in disciplines)
            {
                //if (oneDis.DisciplineStatus == 1)
                if (true)
                {
                    DisciplineViewModel disciplineTemp = new DisciplineViewModel(
                      oneDis.DisciplineID, oneDis.DisciplineName,
                      oneDis.DisciplineStatus, oneDis.Code, oneDis.Description, oneDis.ParentId
                );
                    disciplineTemp.DisciplineCode = oneDis.Code;
                    disciplineTemp.ParentID = oneDis.ParentId;
                    disciplineTemp.Description = oneDis.Description;

                    listDiscipline.Add(disciplineTemp);
                }
            }
            return  listDiscipline;
        }

        public async Task<IList<DisciplineViewModel>> GetDisciplineByParent(Guid parentId)
        {
            var disciplineEntity = _context.Disciplines.Where(x => x.ParentId == parentId).ToList();
            return _mapper.Map<List<DisciplineViewModel>>(disciplineEntity);
        }
    }
}

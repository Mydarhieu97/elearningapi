﻿using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
    public class RespondQueries : IRespondQueries
    {
        private readonly ElearningContext _context;
        public RespondQueries(ElearningContext context)
        {
            _context = context;
        }
        public async Task<RespondViewModel> GetRespond(Guid id)
        {
            var rpID = _context.Responds.Where(r => r.RespondID == id).FirstOrDefault();
           
                RespondViewModel respondForReturn = new RespondViewModel(
                rpID.RespondID,
                rpID.RespondContent,
                rpID.IsTrue,
                rpID.QuestionID,
                rpID.MediaUrl,
                rpID.RespondStatus
                );
            return await Task.FromResult<RespondViewModel>(respondForReturn).ConfigureAwait(true);
        }

        public async Task<List<RespondViewModel>> GetResponds()
        {
            var responds = _context.Responds.AsQueryable();

            List<RespondViewModel> respondsForReturn = new List<RespondViewModel>();

            foreach (var respond in responds)
            {
                RespondViewModel respondTemp = new RespondViewModel(
                respond.RespondID,
                respond.RespondContent,
                respond.IsTrue,
                respond.QuestionID,
                respond.MediaUrl,
                respond.RespondStatus
                );
                respondsForReturn.Add(respondTemp);
            }


            return respondsForReturn;
        }
    }
}

﻿using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
    public class ModuleQueries : IModuleQueries
    {
        private readonly ElearningContext _context;
        public ModuleQueries(ElearningContext context)
        {
            _context = context;
        }
      
      
        public async Task<ModuleViewModel> GetModule(Guid id)
        {
            var oneModule = await _context.Modules.FindAsync(id);

            if (oneModule == null)
                throw new InfrastructureException($"The Module {id} does not exists or is not processed yet.");

            ModuleViewModel moduleForReturn = new ModuleViewModel(
               oneModule.ModuleID, oneModule.ModuleName, oneModule.ModuleStatus
                );
            return await Task.FromResult<ModuleViewModel>(moduleForReturn);
        }

        public async Task<List<ModuleViewModel>> GetListModule()
        {
            var modules =  _context.Modules.AsQueryable();
            List<ModuleViewModel> listModule = new List<ModuleViewModel>();
            foreach (var oneModule in modules)
            {
                if (oneModule.ModuleStatus == 1)
                {
                    ModuleViewModel moduleTemp = new ModuleViewModel(
                      oneModule.ModuleID,
                      oneModule.ModuleName,
                      oneModule.ModuleStatus
                );
                    listModule.Add(moduleTemp);
                }
            }
            return listModule;
        }
    }
}

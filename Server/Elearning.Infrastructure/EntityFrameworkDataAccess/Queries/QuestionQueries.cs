﻿using Elearning.Application.Queries;
using Elearning.Application.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.EntityFrameworkDataAccess.Queries
{
    public class QuestionQueries : IQuestionQueries
    {
        private readonly ElearningContext _context;
        private readonly IRespondQueries _respondQueries;

        public QuestionQueries(ElearningContext context, IRespondQueries respondQueries)
        {
            _context = context;
            _respondQueries = respondQueries;
        }

        public async Task<QuestionViewModel> GetQuestion(Guid id)
        {
            var question = await _context.Questions.FindAsync(id);

            if (question == null)
            {
                throw new UserNotFoundException($"The Question {id} does not exists or is not processed yet.");
            }

            List<Entities.Respond> responds = await _context.Responds.Where(r => r.QuestionID == question.QuestionID).ToListAsync().ConfigureAwait(true);

            List<RespondViewModel> respondsResult = new List<RespondViewModel>();

            foreach (Entities.Respond respond in responds)
            {
                RespondViewModel respondResult = await _respondQueries.GetRespond(respond.RespondID);
                respondsResult.Add(respondResult);
            }

            QuestionViewModel questionForReturn = new QuestionViewModel(
                question.QuestionID,
                question.QuestionContent,
                question.MediaUrl,
                question.CreatedDate,
                question.PersonCreated,
                question.IsLock,
                question.QuestionTypeID,
                respondsResult
                );

            return await Task.FromResult<QuestionViewModel>(questionForReturn);
        }


        public Task<List<QuestionViewModel>> GetQuestions()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Elearning.Infrastructure.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Elearning.Infrastructure
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public ICollection<Order> Orders { get; set; }
        public ICollection<TestAttemp> TestAttemps { get; set; }
        public ICollection<ApplicationUserRole> ApplicationUserRoles { get; set; }
    }

    public class ApplicationRole : IdentityRole<Guid>
    {
        public ICollection<ApplicationUserRole> ApplicationUserRoles { get; set; }
    }
    public class ApplicationUserRole : IdentityUserRole<Guid>
    {
        public ApplicationUser ApplicationUser { get; set; }
        public ApplicationRole ApplicationRole { get; set; }
    }
    public class ElearningContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid,
                                    IdentityUserClaim<Guid>, ApplicationUserRole, IdentityUserLogin<Guid>,
                                    IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public ElearningContext(DbContextOptions options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUserRole>(userRoles =>
            {
                userRoles.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRoles.HasOne(ur => ur.ApplicationRole)
                         .WithMany(r => r.ApplicationUserRoles)
                         .HasForeignKey(ur => ur.RoleId)
                         .IsRequired();
                userRoles.HasOne(ur => ur.ApplicationUser)
                         .WithMany(r => r.ApplicationUserRoles)
                         .HasForeignKey(ur => ur.UserId)
                         .IsRequired();
            });

            modelBuilder.Entity<TestExamOfEvent>().HasKey(ur => new { ur.TestExamID, ur.EventID });
            modelBuilder.Entity<TestExamOfEvent>()
                .HasOne(x => x.Event)
                .WithMany(u => u.TestExamOfEvent)
                .HasForeignKey(x => x.EventID)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TestExamOfEvent>()
                .HasOne(x => x.TestExam)
                .WithMany(u => u.TestExamOfEvent)
                .HasForeignKey(x => x.TestExamID)
                .OnDelete(DeleteBehavior.Restrict);

           // modelBuilder.Entity<TestAttempAswer>().HasKey(ur => new { ur.TestAttempID, ur.QuestionID });
            modelBuilder.Entity<TestAttempAswer>()
                .HasOne(x => x.TestAttemp)
                .WithMany(u => u.TestAttempAswers)
                .HasForeignKey(x => x.TestAttempID)
                .OnDelete(DeleteBehavior.Restrict);
            //modelBuilder.Entity<TestAttempAswer>()
            //    .HasOne(x => x.Question)
            //    .WithMany(u => u.TestAttempAswers)
            //    .HasForeignKey(x => x.QuestionID)
            //    .OnDelete(DeleteBehavior.Restrict);
        }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Module> Modules { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderTestExam> OrderTestExams { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionType> QuestionTypes { get; set; }
        public DbSet<Respond> Responds { get; set; }
        public DbSet<TestAttemp> TestAttemps { get; set; }
        public DbSet<TestAttempAswer> TestAttempAswers { get; set; }
        public DbSet<TestExamQuestion> TestExamQuestions { get; set; }
        public DbSet<TestExamOfEvent> TestExamOfEvent { get; set; }
        public DbSet<TestExam> TestExams { get; set; }
    }
}

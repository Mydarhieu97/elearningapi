﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Elearning.Domain.ElearningEnum;

namespace Elearning.Infrastructure.Entities
{
    public class TestExam
    {
        public Guid TestExamID { get; set; }

        public string TestExamName { get; set; }
        public ExamStatus TestExamStatus { get; set; }
        public Guid DisciplineID { get; set; }
        public Discipline Disciplines { get; set; }
        public Guid ModuleID { get; set; }
        public Module Modules { get; set; }
        public ICollection<TestExamQuestion> TestExamQuestions { get; set; }
        public ICollection<TestExamOfEvent> TestExamOfEvent { get; set; }
        public ICollection<OrderTestExam> OrderTestExams { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class Discipline
    {
        [Key]
        public Guid DisciplineID { get; set; }
        public string DisciplineName { get; set; }
        public int DisciplineStatus { get; set; }
        public ICollection<TestExam> TestExams { get; set; }
        public string Code { get; set; }
        public Guid? ParentId { get; set; }
        public string Description { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class QuestionType
    {
        public Guid QuestionTypeID { get; set; }
        public string QuestionTypeName { get; set; }
        public int QuestionTypeStatus { get; set; }
        public ICollection<Question> Questions { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class Module
    {
        public Guid ModuleID { get; set; }
        public string ModuleName { get; set; }
        public int ModuleStatus { get; set; }
        public ICollection<TestExam>  TestExams { get; set; }
        public string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class Respond
    {
        public Guid RespondID { get; set; }
        public string RespondContent { get; set; }
        public bool IsTrue { get; set; }
        public Guid QuestionID { get; set; }
        public Question Question { get;set;}
        public string MediaUrl { get; set; }
        public int RespondStatus { get; set; }
    }
}

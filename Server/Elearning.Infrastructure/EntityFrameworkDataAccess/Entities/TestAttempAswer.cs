﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class TestAttempAswer
    {
        public Guid TestAttempAswerID { get; set; }
        public Guid TestAttempID { get; set; }
        public TestAttemp TestAttemp { get; set; }
    }
}

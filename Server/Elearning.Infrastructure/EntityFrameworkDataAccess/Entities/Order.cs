﻿using System;
using System.Collections.Generic;

namespace Elearning.Infrastructure.Entities
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public DateTime OrderTime { get; set; }
        public ApplicationUser User { get; set; }
        public Guid UserId { get; set; }
        public int OrderStatus { get; set; }
        public ICollection<OrderTestExam> OrderTestExams { get; set; }
    }
}

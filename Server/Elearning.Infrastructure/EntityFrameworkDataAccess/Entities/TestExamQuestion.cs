﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class TestExamQuestion
    {
        public Guid TestExamQuestionID { get; set; }
        public Guid TestExamID { get; set; }
        public TestExam TestExam { get; set; }
        public Guid QuestionID { get; set; }
        public Question Question { get; set; }
        public double  TestExamQuestionFactor { get; set; }
        public double TestExamQuestionMark { get; set; }
        public int TestExamQuestionStatus { get; set; }

    }
}

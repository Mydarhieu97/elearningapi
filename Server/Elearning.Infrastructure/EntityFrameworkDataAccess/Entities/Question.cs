﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class Question
    {
        public Guid QuestionID { get; set; }
        public string QuestionContent { get; set; }
        public string MediaUrl { get; set; }
        
        public DateTime CreatedDate { get; set; }
        public Guid PersonCreated { get; set; }
        public bool IsLock { get; set; }
        public QuestionType QuestionType { get; set; }
        public Guid QuestionTypeID { get; set; }
        public ICollection<Respond> Responds { get; set; }
        public ICollection<TestExamQuestion> TestExamQuestions { get; set; }
    }
}

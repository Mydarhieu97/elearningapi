﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class TestAttemp
    {
        public Guid TestAttempID { get; set; }
        public DateTime TestAttemTime { get; set; }
        public int TestAttemStatus { get; set; }
        public double TotalMark { get; set; }
        public string EnrollCode { get; set; }
        public Guid OrderTestExamID { get; set; }
        public OrderTestExam OrderTestExam { get; set; }
        public ICollection<TestAttempAswer>  TestAttempAswers { get; set; }
        public ApplicationUser User { get; set; }
        public long Price { get; set; }
    }
}

﻿using System;

namespace Elearning.Infrastructure.Entities
{
    public class OrderTestExam
    {
        public Guid OrderTestExamId { get; set; }
        public Guid OrderId { get; set; }
        public Guid TestExamId { get; set; }
        public int OrderTestExamStatus { get; set; }
        public TestExam TestExam { get; set; }
        public Order Order { get; set; }
        public TestAttemp TestAttemp { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class Event
    {
        public Guid EventID { get; set; }
        public string EventName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EventDescription { get; set; }
        public Guid PersonCreated { get; set; } //PersonCreated
        public ICollection<TestExamOfEvent> TestExamOfEvent { get; set; }

    }
}

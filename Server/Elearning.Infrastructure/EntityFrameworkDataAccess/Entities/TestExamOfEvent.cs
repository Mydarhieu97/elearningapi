﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Elearning.Infrastructure.Entities
{
    public class TestExamOfEvent
    {
        public Guid TestExamOfEventID { get; set; }
        public int Status { get; set; }
        public Guid TestExamID { get; set; }
        public TestExam TestExam { get; set; }
        public Guid EventID { get; set; }
        public Event Event { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Elearning.Infrastructure
{
    public class UserNotFoundException : InfrastructureException
    {
        internal UserNotFoundException(string message)
            : base(message)
        { }
    }
}
